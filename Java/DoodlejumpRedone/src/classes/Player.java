package classes;

import java.awt.Dimension;
import java.awt.Point;

public class Player
{
	private Game mainReference;
	private Pane paneReference;

	private Point mousePos; // relative to center of pane

	private double x, y, vx, ax, max_vx, min_x, max_x;
	private int w, h;

	private double elevation, vy, ay;

	private double nozzleAngleSet, nozzleAngle, minNozzleAngle, maxNozzleAngle, stepNozzleAngle;
	private int frameW;

	private boolean right, left;

	private double jetpack;

	public Player(int w, int h, Game mainReference, Pane paneReference)
	{
		this.w = w;
		this.h = h;
		this.mainReference = mainReference;
		this.paneReference = paneReference;

		frameW = (int) Reference.STARTSIZE.getWidth();

		x = 0;
		y = Reference.PLAYER_Y;
		vx = 0;
		vy = Reference.PHYSICS_START_VY;
		ax = Reference.PLAYER_A;
		ay = Reference.PHYSICS_A;
		max_vx = Reference.PLAYER_MAX_VX;
		min_x = -frameW / 2;
		max_x = frameW / 2;

		minNozzleAngle = Math.toRadians(Reference.NOZZLE_MIN_A);
		maxNozzleAngle = Math.toRadians(Reference.NOZZLE_MAX_A);
		stepNozzleAngle = Math.toRadians(Reference.NOZZLE_STEP_A);

		mousePos = new Point();
	}

	public void updateValues(boolean l, boolean r, Point mP)
	{
		left = l;
		right = r;

		mousePos.setLocation(mP.getX() - x, mP.getY() - y + +h / 2);
	}

	public void move(int interval)
	{
		double f = interval / 1000d;

		handleJetpack(f);

		if (left && !right)
			vx -= ax * f;
		else if (!left && right)
			vx += ax * f;
		else if (vx >= ax * f)
			vx -= ax * f;
		else if (vx <= -ax * f)
			vx += ax * f;
		else
			vx = 0;

		if (vx > max_vx)
			vx = max_vx;
		else if (vx < -max_vx)
			vx = -max_vx;

		x += vx * f;

		if (x > max_x)
			x -= frameW;
		else if (x < min_x)
			x += frameW;

		nozzleAngleSet = Math.atan2(mousePos.getY() - paneReference.getCameraOffset().getY(),
				mousePos.getX() - paneReference.getCameraOffset().getX());

		double diff = nozzleAngle - nozzleAngleSet;
		if (diff > Math.PI)
			diff -= 2 * Math.PI;
		else if (diff < -Math.PI)
			diff += 2 * Math.PI;

		if (Math.abs(diff) <= stepNozzleAngle / 2 * f)
		{
			nozzleAngle = nozzleAngleSet;
		} else if (diff < 0)
		{
			nozzleAngle += stepNozzleAngle * f;
		} else
		{
			nozzleAngle -= stepNozzleAngle * f;
		}

		// Physics
		vy += ay * f;
		elevation += vy * f;
	}

	private void handleJetpack(double f)
	{
		if (jetpack > 0.1 || jetpack < -0.1)
			ay = -5 * Reference.PHYSICS_A;
		else
			ay = Reference.PHYSICS_A;

		if (jetpack > 0)
			jetpack -= f;

		if (jetpack < f && jetpack > -1)
			jetpack = 0;
	}

	public void jetpack(int n)
	{
		jetpack = n;
	}

	public Projectile shoot()
	{
		switch (mainReference.getThemeDoodle())
		{
		case 1:
			return new Projectile(new Point((int) x, (int) (elevation - getHeight() / 2)), nozzleAngle, 10,
					ProjectileType.PEA, this);

		case 2:
			return new Projectile(new Point((int) x, (int) (elevation - getHeight() / 2)), nozzleAngle, 10,
					ProjectileType.LASER, this);
		}
		return null;
	}

	public void setElevation(int elevation)
	{
		this.elevation = elevation;
	}

	public double getElevation()
	{
		return elevation;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public double getX()
	{
		return x;
	}

	public double getY()
	{
		return y;
	}

	public int getWidth()
	{
		return w;
	}

	public int getHeight()
	{
		return h;
	}

	public void setVx(int vx)
	{
		this.vx = vx;
	}

	public double getVx()
	{
		return vx;
	}

	public void setVy(int vy)
	{
		this.vy = vy;
	}

	public double getVy()
	{
		return vy;
	}

	public Dimension getSize()
	{
		return new Dimension(w, h);
	}

	public double getNozzleAngle()
	{
		return nozzleAngle;
	}
}