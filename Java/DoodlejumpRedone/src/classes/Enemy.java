package classes;

import java.awt.Dimension;

public class Enemy
{
	private int elevation;
	private EnemyType type;
	private int x;
	private int hp = 1;
	private boolean stationary = true;

	private int wiggleOffset = 0;
	private int maxWiggleOffset = 0;

	private Dimension size;

	public Enemy(int e)
	{
		this.elevation = e;
		this.maxWiggleOffset = 25;
		this.size = (this.type = EnemyType.THE_FUCKMAN).getSize();
	}

	public Enemy(int e, int x, EnemyType t)
	{
		this(e);
		this.x = x;
		this.size = (this.type = t).getSize();
	}

	public Enemy(int e, int x, EnemyType t, int hp, int mwo)
	{
		this(e, x, t);
		this.hp = hp;
		this.maxWiggleOffset = mwo;
	}

	public void fuckOrGetFucked(Player p)
	{
		if (overlap(p))
		{
			if (p.getVy() > 0) // The Enemy got fucking fucked
			{
				hp = 0;
				p.setVy(Reference.ENEMY_BOUNCE_SPEED);

			} else // The Player got fucking fucked
			{
				p.setElevation(Reference.DEATH_DISTANCE + 1);
			}
		}
	}

	public void getShot(Projectile p)
	{
		hp--;
		p.dispose();
	}

	private boolean overlap(Player p)
	{
		return p.getX() + p.getWidth() / 2 > getX() && p.getX() - p.getWidth() / 2 < getX() + getWidth()
				&& p.getElevation() > getElevation() && p.getElevation() < getElevation() + getHeight();
	}

	public int getX()
	{
		return this.x;
	}

	public Dimension getSize()
	{
		return size;
	}

	public int getWidth()
	{
		return (int) size.getWidth();
	}

	public int getHeight()
	{
		return (int) size.getHeight();
	}

	public int getElevation()
	{
		return elevation;
	}

	public boolean dead()
	{
		return hp <= 0;
	}
}
