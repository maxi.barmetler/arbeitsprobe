package classes;

public class Tools
{
	public static double constrain(double n, double min, double max)
	{
		if (min > max)
		{
			double temp = min;
			min = max;
			max = temp;
		}
		if (n > max)
			return max;
		if (n < min)
			return min;
		return n;
	}
}