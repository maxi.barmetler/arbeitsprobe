package classes;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Game extends JFrame
{
	private static final Toolkit tk = Toolkit.getDefaultToolkit();
	private static final int shootCooldown = Reference.PLAYER_SHOOT_COOLDOWN;

	private Cursor cursor, pointer;

	private int gameInterval = Reference.GAME_INTERVAL;
	private long lastStep = -1;
	private long lastShot = -1;
	private Pane pane;

	private ArrayList<String> keys;

	private Player player;
	private ArrayList<Platform> platforms;
	private ArrayList<Projectile> projectiles;
	private ArrayList<Enemy> enemies;

	private int score;
	private boolean pause = false;
	private boolean dead = false;
	private double deathTime = -1;

	private int themeWorld = 1;
	private int themeDoodle = 1;

	Game()
	{
		cursor = tk.createCustomCursor(new ImageIcon(getClass().getResource("../images/cursor.png")).getImage(), new Point(16, 16), "cursor");
		pointer = tk
				.createCustomCursor(new ImageIcon(getClass().getResource("../images/pointer.png")).getImage(), new Point(16, 16), "pointer");

		setCursor(pointer);
		setSize(new Dimension(200, 100));
		setResizable(false);
		setTitle("Motherf*cking Doodlejump!");
		setIconImage(new ImageIcon(getClass().getResource("../images/icon.png")).getImage());
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		Thread updateThread = new Thread(() -> {
			while (true)
				tick();
		});
		pane = new Pane();
		Thread paneThread = new Thread(pane);
		setContentPane(pane);
		setupMenuBar();
		pack();
		setLocationRelativeTo(null);

		setupInput();
		requestFocus();

		setupGame();

		setVisible(true);
		updateThread.start();
		paneThread.start();
	}

	public void tick()
	{
		Point mP = MouseInfo.getPointerInfo().getLocation();
		mP.translate((int) (-pane.getLocationOnScreen().getX() - pane.getWidth() / 2),
				(int) (-pane.getLocationOnScreen().getY() - pane.getHeight() / 2));
		pane.updateValues(keys, player, mP, score, pause, dead, themeWorld, themeDoodle);
		player.updateValues(keys.contains(KeyEvent.VK_A + "") || keys.contains(KeyEvent.VK_LEFT + ""),
				keys.contains(KeyEvent.VK_D + "") || keys.contains(KeyEvent.VK_RIGHT + ""), mP);
		if (keys.contains(KeyEvent.VK_SPACE + ""))
			player.jetpack(-1);
		else
			player.jetpack(0);
		checkCommands();

		if ((lastStep == -1 || System.currentTimeMillis() - lastStep >= gameInterval) && !pause)
		{
			player.move(gameInterval);
			for (Platform p : platforms)
				p.move(gameInterval);

			updatePlatforms();
			updateProjectiles();
			if (-player.getElevation() > score * 10)
			{
				score = (int) (-player.getElevation() / 10d);
			} else if (-player.getElevation() < (score - Reference.DEATH_DISTANCE) * 10 && !dead)
			{
				System.out.println("dieded");
				dead = true;
				setCursor(cursor);
				deathTime = System.currentTimeMillis();
			}

			if (deathTime > 0 && System.currentTimeMillis() > deathTime + Reference.DEATH_COOLDOWN)
			{
				System.out.println("resurrected");
				restart();
				deathTime = -1;
			}

			lastStep = System.currentTimeMillis();
		}
	}

	private void restart()
	{
		dead = false;
		setCursor(pointer);
		platforms.clear();
		player.setElevation(0);
		score = 0;
		player.setVy(Reference.PHYSICS_START_VY);
		player.setX(0);
		player.setVx(0);
	}

	private boolean wasEsc = false;

	private void checkCommands()
	{
		boolean esc = keys.contains(KeyEvent.VK_ESCAPE + "");

		if (!wasEsc && esc && !dead)
			switchPause();

		wasEsc = esc;
	}

	private void switchPause()
	{
		pause ^= true;
		if (pause)
			setCursor(cursor);
		else if (!dead)
			setCursor(pointer);
	}

	private void updatePlatforms()
	{
		int e = (int) player.getElevation();

		boolean enemy = Math.random() < Reference.ENEMY_PROBABILITY;

		if (platforms.isEmpty())
		{
			platforms.add(createPlatform(500));

		} else if (platforms.get(platforms.size() - 1).getMiny() + player.getY() > -pane.getHeight() / 2)
		{
			platforms.add(createPlatform(platforms.get(platforms.size() - 1).getMinElevation()));
		}

		if (!platforms.isEmpty())
		{
			if (platforms.get(0).getMiny() + player.getY() > pane.getHeight() / 2 + Reference.PLATFORM_DEATH)
			{
				platforms.remove(0);
			}
		}

		platforms.forEach(p -> {
			p.updateY(e);
			p.bounce(player);
		});

		pane.platforms = (ArrayList<Platform>) platforms.clone();
		pane.enemies = (ArrayList<Enemy>) enemies.clone();
	}

	private void updateProjectiles()
	{
		boolean shoot = keys.contains("action_1");

		if (shoot && System.currentTimeMillis() - lastShot >= shootCooldown)
		{
			projectiles.add(player.shoot());
			lastShot = System.currentTimeMillis();
		}

		int e = (int) player.getElevation();

		for (int i = 0; i < projectiles.size(); i++)
		{
			Projectile p = projectiles.get(i);
			if (p.dead())
				projectiles.remove(p);
			p.updateY(e);
			p.move(gameInterval);
		}

		pane.projectiles = (ArrayList<Projectile>) projectiles.clone();
	}

	private Platform createPlatform(int lastElevation)
	{
		int difficulty = (int) (100d - (80 * Math.pow(1.8, -score / 1000d)));
		int dy = (int) (Reference.PLATFORM_DY * (1 - Math.random() * .5) * difficulty / 100d);
		int x = (int) ((pane.getWidth() - Reference.PLATFORM_SIZE.getWidth()) * (Math.random() - .5));

		double chanceBounce = .075;
		double chanceMove = difficulty * .5 / 100d;
		double chanceVer = .25;

		boolean bounce = Math.random() < chanceBounce;

		if (Math.random() < chanceMove)
		{
			int speed = (int) (Reference.PLATFORM_MAX_MOVESPEED * (1 - difficulty / 200d));
			if (Math.random() < chanceVer)
			{
				int amount = (int) (Math.random() * 150d + 75 * (1 - difficulty / 200d));
				return new Platform(x, lastElevation - dy - amount, speed, amount, PlatformMoveType.VERTICAL);
			}
			if (bounce)
				return new Platform(x, lastElevation - dy, speed,
						(int) ((pane.getWidth() - Reference.PLATFORM_SIZE.getWidth()) / 2d), PlatformType.BOUNCE,
						PlatformMoveType.HORIZONTAL);
			return new Platform(x, lastElevation - dy, speed,
					(int) ((pane.getWidth() - Reference.PLATFORM_SIZE.getWidth()) / 2d), PlatformMoveType.HORIZONTAL);
		}

		if (bounce)
			return new Platform(x, lastElevation - dy, PlatformType.BOUNCE);
		return new Platform(x, lastElevation - dy);
	}

	private void setupMenuBar()
	{
		// MenuBar
		JMenuBar menuBar = new JMenuBar();
		JMenu menu = new JMenu("Settings");
		menuBar.add(menu);

		menu = new JMenu("Themes");
		menuBar.getMenu(0).add(menu);

		JMenuItem menuItem = new JMenuItem("Classic");
		menuItem.addActionListener(e -> themeWorld = 1);
		menu.add(menuItem);
		menuItem = new JMenuItem("Space");
		menuItem.addActionListener(e -> themeWorld = 2);
		menu.add(menuItem);

		menu = new JMenu("Doodles");
		menuBar.getMenu(0).add(menu);

		menuItem = new JMenuItem("Classic");
		menuItem.addActionListener(e -> themeDoodle = 1);
		menu.add(menuItem);
		menuItem = new JMenuItem("Space");
		menuItem.addActionListener(e -> themeDoodle = 2);
		menu.add(menuItem);

		setJMenuBar(menuBar);
	}

	private void setupGame()
	{
		player = new Player(64, 64, this, pane);
		platforms = new ArrayList<Platform>();
		projectiles = new ArrayList<Projectile>();
		enemies = new ArrayList<Enemy>();
		pane.platforms = (ArrayList<Platform>) platforms.clone();
		pane.projectiles = (ArrayList<Projectile>) projectiles.clone();
		pane.enemies = (ArrayList<Enemy>) enemies.clone();
	}

	private void setupInput()
	{
		keys = new ArrayList<String>();

		addKeyListener(new KeyAdapter()
		{

			@Override public void keyTyped(KeyEvent e)
			{
			}

			@Override public void keyReleased(KeyEvent e)
			{
				keys.remove(e.getKeyCode() + "");
			}

			@Override public void keyPressed(KeyEvent e)
			{

				if (!keys.contains(e.getKeyCode() + ""))
					keys.add(e.getKeyCode() + "");
			}
		});

		addMouseListener(new MouseListener()
		{
			@Override public void mouseReleased(MouseEvent e)
			{
				keys.remove("action_" + e.getButton());
			}

			@Override public void mousePressed(MouseEvent e)
			{
				if (!keys.contains("action_" + e.getButton()))
					keys.add("action_" + e.getButton());
			}

			@Override public void mouseExited(MouseEvent e)
			{
			}

			@Override public void mouseEntered(MouseEvent e)
			{
			}

			@Override public void mouseClicked(MouseEvent e)
			{
			}
		});
	}

	public int getThemeWorld()
	{
		return themeWorld;
	}

	public int getThemeDoodle()
	{
		return themeDoodle;
	}
}