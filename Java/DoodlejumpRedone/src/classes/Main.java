package classes;

public class Main
{
	public static Game Instance;

	public static void main(String[] args)
	{
		Images.init(Game.class.getResource("../images").getPath());
		Reference.init();
		Instance = new Game();
	}
}
