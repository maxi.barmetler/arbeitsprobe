package classes;

public enum ProjectileType
{
	PEA, BALL, LASER
}