package classes;

import java.awt.Point;

public class Projectile extends Point
{
	private double vx, vy, dir;
	private ProjectileType type;
	private int elevation;
	private boolean dead;
	private long birth;
	private int lifeLength = 1000;
	private double damage; // TODO

	public Projectile(Point startPos, double dir, double damage)
	{
		this(startPos, dir, damage, ProjectileType.PEA);
	}

	public Projectile(Point startPos, double dir, double damage, ProjectileType type)
	{
		super(startPos);
		this.setDir(dir);
		this.type = type;
		this.elevation = y;
		this.damage = damage;
		dead = false;
		birth = System.currentTimeMillis();

		double speed = Reference.PROJECTILE_SPEED.containsKey(type) ? Reference.PROJECTILE_SPEED.get(type) : 0;

		vx = Math.cos(dir) * speed;
		vy = Math.sin(dir) * speed;

		switch (type)
		{
		case BALL:
		case PEA:
			setDir(0);
		}
	}

	public Projectile(Point startPos, double dir, double damage, ProjectileType type, Player player)
	{
		this(startPos, dir, damage, type);
		vy += player.getVy();
	}

	public boolean dead()
	{
		if (dead)
			return true;
		if (System.currentTimeMillis() - birth >= lifeLength)
			dead = true;
		return dead;
	}
	
	public void dispose()
	{
		dead = true;
	}

	public void move(int interval)
	{
		double f = interval / 1000d;

		int gravity = 0;

		switch (type)
		{
		case BALL:
			gravity = Reference.PHYSICS_A;
			break;
		}
		vy += gravity * f;

		x += vx * f;
		elevation += vy * f;
	}

	public void updateY(int elevation)
	{
		y = this.elevation - elevation;
	}

	public int getElevation()
	{
		return elevation;
	}

	public double getDir()
	{
		return dir;
	}

	public void setDir(double dir)
	{
		this.dir = dir;
	}
}