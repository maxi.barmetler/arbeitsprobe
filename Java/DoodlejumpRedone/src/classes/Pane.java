package classes;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.IntStream;

import javax.swing.JPanel;

public class Pane extends JPanel implements Runnable
{
	private long interval = 10;

	private ArrayList<String> keys;
	public ArrayList<Platform> platforms;
	public ArrayList<Projectile> projectiles;
	public ArrayList<Enemy> enemies;

	private Point mousePos; // relative to center of pane

	// Camera
	int offsetX = 0;
	int offsetY = 0;
	int[] offsetYList;
	int smoothStep = 0;
	final int smoothSteps = 10;

	private int playerX = 0, playerY = 0;
	private int playerW = 0, playerH = 0;
	private double nozzleAngle = 0; // 0 means right, PI/2 means down etc.

	private int centerX = 0, centerY = 0;

	private int elevation = 0;
	private int score;
	private boolean pause;
	private boolean dead;

	private int theme = 1;
	private int themed = 1;

	public Pane()
	{
		offsetYList = new int[smoothSteps];
		setPreferredSize(Reference.STARTSIZE);
		setFocusable(false);
		keys = new ArrayList<String>();
		platforms = new ArrayList<Platform>();
		projectiles = new ArrayList<Projectile>();

		playerY = Reference.PLAYER_Y;
		centerX = (int) (Reference.STARTSIZE.getWidth() / 2);
		centerY = (int) (Reference.STARTSIZE.getHeight() / 2);
		mousePos = new Point();
	}

	public void updateValues(ArrayList<String> k, Player player, Point mP, int score, boolean pause, boolean dead,
			int theme, int themed)
	{
		mousePos.setLocation(mP);
		keys = k;
		playerX = (int) player.getX();
		playerY = (int) player.getY();
		playerW = (int) player.getSize().getWidth();
		playerH = (int) player.getSize().getHeight();
		nozzleAngle = player.getNozzleAngle();
		elevation = (int) player.getElevation();
		this.score = score;
		this.pause = pause;
		this.dead = dead;
		this.theme = theme;
		this.themed = themed;

		if (!pause)
			smoothCamera((int) player.getVy() / 2);
		if (dead)
			smoothCamera((int) player.getVy() * 5);
	}

	public Point getCameraOffset()
	{
		return new Point(offsetX, offsetY);
	}

	@Override public void run()
	{
		while (true)
		{
			repaint();
			try
			{
				Thread.sleep(interval);
			} catch (InterruptedException ignored)
			{
			}
		}
	}

	private void smoothCamera(int setPoint)
	{
		offsetYList[smoothStep] = (int) (setPoint / 20d);

		offsetY = IntStream.of(offsetYList).sum() / smoothSteps;
	}

	@Override public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		Map<String, BufferedImage> doodleImg = Images
				.getImgs("doodle", themed, Arrays.asList("body", "nozzle", "eye", "helmet"));

		if (!pause && !dead)
		{
			if (smoothStep == smoothSteps - 1)
				smoothStep = 0;
			else
				smoothStep++;
		}

		// Background
		int backgroundH = Images.getImg("background", theme).getHeight();
		int backgroundY = (int) (-elevation / 10d + getHeight() - backgroundH);

		while (backgroundY > getHeight())
			backgroundY -= backgroundH;

		int backgroundAmount = 1;
		while (backgroundY - (backgroundAmount - 1) * backgroundH > 0)
			backgroundAmount++;

		for (int i = 0; i < backgroundAmount; i++)
			g.drawImage(Images.getImg("background", theme), 0, backgroundY - i * backgroundH, getWidth(), backgroundH,
					this);
		//
		//
		//

		Graphics2D nozzleG = (Graphics2D) g.create();
		Graphics2D projectileG = (Graphics2D) g.create();

		for (Platform p : platforms)
		{
			if (p.isBounce())
			{
				g.drawImage(Images.getImg("spring", theme), (int) (centerX + p.getX() + offsetX + p.getWidth() / 2
								- Images.getImg("spring", theme).getWidth() / 2),
						(int) (centerY + p.getY() + playerY + offsetY - Images.getImg("spring", theme).getHeight()),
						this);

			}
			String type;
			switch (p.moveType())
			{
			case HORIZONTAL:
			case VERTICAL:
				type = p.moveType().name().toLowerCase();
				break;
			default:
				type = "";
			}
			g.drawImage(Images.getImg("platform" + type, theme), (int) (centerX + p.getX() + offsetX),
					(int) (centerY + p.getY() + playerY + offsetY), (int) p.getWidth(),
					(int) (p.getWidth() * Images.getImg("platform" + type, theme).getHeight() / Images
							.getImg("platform" + type, theme).getWidth()), this);

		}

		for (Enemy p : enemies)
		{
			g.drawImage(Images.getImg("projectile", themed),
					(int) (centerX + p.getX() - Images.getImg("projectile", themed).getWidth() / 2.0),
					(int) (centerY + -Images.getImg("projectile", themed).getHeight() / 2.0),
					Images.getImg("projectile", themed).getWidth(), Images.getImg("projectile", themed).getHeight(),
					this);
		}

		for (Projectile p : projectiles)
		{
			projectileG.translate(centerX + p.getX() + offsetX, centerY + p.getY() + playerY + offsetY);
			projectileG.rotate(p.getDir());
			projectileG.drawImage(Images.getImg("projectile", themed),
					(int) (-Images.getImg("projectile", themed).getWidth() / 2.0),
					(int) (-Images.getImg("projectile", themed).getHeight() / 2.0),
					Images.getImg("projectile", themed).getWidth(), Images.getImg("projectile", themed).getHeight(),
					this);
			projectileG.rotate(-p.getDir());
			projectileG.translate(-(centerX + p.getX() + offsetX), -(centerY + p.getY() + playerY + offsetY));
		}

		if (!pause)
			g.drawLine(centerX + offsetX + playerX, centerY + playerY - playerH / 2 + offsetY,
					(int) (mousePos.getX() + centerX), (int) (mousePos.getY() + centerY));

		g.drawImage(doodleImg.get("body"), centerX + offsetX + playerX - playerW / 2,
				centerY + offsetY + playerY - playerH, playerW, playerH, this);

		nozzleG.translate(centerX + offsetX + playerX, (centerY + offsetY + playerY - playerH / 2.0));
		nozzleG.rotate(nozzleAngle);

		nozzleG.drawImage(doodleImg.get("nozzle"), -doodleImg.get("nozzle").getWidth() / 2,
				-doodleImg.get("nozzle").getHeight() / 2, this);

		int max = 10;
		double eyeX = Tools.constrain((mousePos.getX() - playerX) / 30, -max, max);
		double eyeY = Tools.constrain((mousePos.getY() - playerY + playerH / 2) / 30, -max, max);

		g.drawImage(doodleImg.get("eye"),
				(int) (centerX + playerX + offsetX - doodleImg.get("eye").getWidth() / 2 + eyeX),
				(int) (centerY + offsetY + playerY - doodleImg.get("eye").getHeight() / 2
						- Images.getImg("doodle", themed, "body").getHeight() / 2 + eyeY), this);

		if (doodleImg.get("helmet") != null)
		{
			g.drawImage(doodleImg.get("helmet"), centerX + offsetX + playerX - doodleImg.get("helmet").getWidth() / 2,
					centerY + offsetY + playerY - doodleImg.get("helmet").getHeight(), playerW, playerH, this);
		}
		g.drawString(keys.toString(), 10, 150);
		g.drawString("" + score, getWidth() - g.getFontMetrics().stringWidth("" + score) - 10, 150);

		if (pause)
		{
			g.setColor(new Color(0, 0, 0, 150));
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(new Color(220, 220, 220));
			g.setFont(new Font("Default", Font.PLAIN, 64));
			String s = "PAUSED";
			g.drawString(s, centerX - g.getFontMetrics().stringWidth(s) / 2, centerY);
		} else if (dead)
		{
			g.drawImage(Images.getImg("ded"), 0, 0, this);
		}
	}
}

//
//
//
//