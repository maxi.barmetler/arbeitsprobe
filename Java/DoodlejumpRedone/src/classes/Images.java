package classes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

public class Images
{
	private static Map<String, BufferedImage> images;

	public static void init(String dir)
	{
		images = new HashMap<>();
		File folder = new File(dir);
		if (!folder.isDirectory())
		{
			System.out.println("You've been fricked!");
			return;
		}
		File[] files = folder.listFiles((dir1, name) -> name.endsWith(".png"));

		System.out.println("Loading Images...");

		for (File f : files)
		{
			String name = f.getName();
			name = name.substring(0, name.length() - 4);
			try
			{
				images.put(name, ImageIO.read(f));
			} catch (IOException e)
			{
				System.out.println("Failed to load " + name + ".png!");
			}
			System.out.println("Loaded " + name + ".png;");
		}

		System.out.println("done!");
	}

	public static Map<String, BufferedImage> getImgs(String name, int theme, List<String> variants)
	{
		Map<String, BufferedImage> output = new HashMap<>();

		variants.forEach(variant ->
		{
			String t = theme + "";
			if (t.length() < 2)
				t = "0" + t;
			String key = name + "_" + t + "_" + variant;
			output.put(variant, images.get(key));
		});

		return output;
	}

	public static BufferedImage getImg(String name)
	{
		return images.get(name);
	}

	public static BufferedImage getImg(String name, int theme)
	{
		String t = theme + "";
		if (t.length() < 2)
			t = "0" + t;
		String key = name + "_" + t;
		if (theme > 99 || theme < 1 || !images.containsKey(key))
			return images.get("null");
		return getImg(key);
	}

	public static BufferedImage getImg(String name, int theme, String var)
	{
		String t = theme + "";
		if (t.length() < 2)
			t = "0" + t;
		String key = name + "_" + t + "_" + var;
		if (theme > 99 || theme < 1 || !images.containsKey(key))
			return images.get("null");
		return getImg(key);
	}
}