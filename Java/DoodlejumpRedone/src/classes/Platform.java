package classes;

import java.awt.Point;
import java.awt.Rectangle;

public class Platform extends Rectangle
{
	private int elevation, elevationOffset;
	private int bounceSpeed = Reference.PLATFORM_BOUNCE_SPEED_DEFAULT;
	private int velocity;
	private int max;
	private int x;
	private PlatformType type;
	private PlatformMoveType moveType;

	Platform(int x, int e)
	{
		super(new Point((int) (x - Reference.PLATFORM_SIZE.getWidth() / 2), -100), Reference.PLATFORM_SIZE);
		elevation = e;
		this.x = x;
		type = PlatformType.DEFAULT;
		moveType = PlatformMoveType.STILL;
	}

	Platform(int x, int e, PlatformType t)
	{
		super(new Point((int) (x - Reference.PLATFORM_SIZE.getWidth() / 2), -100), Reference.PLATFORM_SIZE);
		elevation = e;
		this.x = x;
		type = t;
		moveType = PlatformMoveType.STILL;
		if (type == PlatformType.BOUNCE)
			this.bounceSpeed = Reference.PLATFORM_BOUNCE_SPEED_BOUNCE;
	}

	Platform(int x, int e, int v, int max, PlatformMoveType mt)
	{
		super(new Point((int) (x - Reference.PLATFORM_SIZE.getWidth() / 2), -100), Reference.PLATFORM_SIZE);
		elevation = e;
		velocity = v;
		type = PlatformType.DEFAULT;
		moveType = mt;
		this.max = max;
		this.x = x;
	}

	Platform(int x, int e, int v, int max, PlatformType t, PlatformMoveType mt)
	{
		super(new Point((int) (x - Reference.PLATFORM_SIZE.getWidth() / 2), -100), Reference.PLATFORM_SIZE);
		elevation = e;
		velocity = v;
		type = t;
		moveType = mt;
		this.max = max;
		this.x = x;
		if (type == PlatformType.BOUNCE)
			this.bounceSpeed = Reference.PLATFORM_BOUNCE_SPEED_BOUNCE;
	}

	void bounce(Player p)
	{
		if (p.getX() + p.getWidth() / 2 > getX() && p.getX() - p.getWidth() / 2 < getX() + getWidth())
		{
			if (p.getElevation() > getElevation() && p.getElevation() < getElevation() + getHeight())
			{
				if (p.getVy() > 0)
				{
					p.setVy(bounceSpeed);
				}
			}
		}
	}

	private int dir = 1;

	void move(int interval)
	{
		if (velocity == 0 || moveType == PlatformMoveType.STILL)
			return;

		double f = interval / 1000d;

		switch (moveType)
		{
		case HORIZONTAL:
			x += (int) (dir * velocity * f);
			if (x > max)
				dir = -1;
			else if (x < -max)
				dir = 1;
			x = (int) Tools.constrain(x, -max, max);
			break;
		case VERTICAL:
		default:
			elevationOffset += dir * velocity * f;
			if (elevationOffset > max)
				dir = -1;
			else if (elevationOffset < -max)
				dir = 1;
			elevationOffset = (int) Tools.constrain(elevationOffset, -max, max);
		}

		setLocation((int) (x - getWidth() / 2), (int) getY());
	}

	public void updateY(int elevation)
	{
		y = this.elevation + elevationOffset - elevation;
	}

	public int getBounceSpeed()
	{
		return bounceSpeed;
	}

	public int getElevation()
	{
		return elevation + elevationOffset;
	}

	public void setElevation(int height)
	{
		this.elevation = height;
	}

	public PlatformType type()
	{
		return type;
	}

	public PlatformMoveType moveType()
	{
		return moveType;
	}

	public boolean isBounce()
	{
		return type == PlatformType.BOUNCE;
	}

	public int getMiny()
	{
		if (moveType == PlatformMoveType.VERTICAL)
			return (int) (getY() - elevationOffset - max);
		return (int) getY();
	}

	public int getMaxy()
	{
		if (moveType == PlatformMoveType.VERTICAL)
			return (int) (getY() - elevationOffset + max);
		return (int) getY();
	}

	public int getMinElevation()
	{
		if (moveType == PlatformMoveType.VERTICAL)
			return -max + elevation;
		return elevation;
	}

	public int getMaxElevation()
	{
		if (moveType == PlatformMoveType.VERTICAL)
			return max + elevation;
		return elevation;
	}
}