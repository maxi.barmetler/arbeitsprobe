package classes;

import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;

public class Reference
{
	public static final Dimension STARTSIZE = new Dimension(700, 900);

	// Player
	public static final int PLAYER_A = 5000;
	public static final int PLAYER_MAX_VX = 700;

	public static final int PLAYER_Y = 150;

	public static final int PLAYER_SHOOT_COOLDOWN = 300;

	// Nozzle
	public static final int NOZZLE_MIN_A = -135, NOZZLE_MAX_A = -45; // here degrees, will be converted
	public static final int NOZZLE_STEP_A = 360;

	// Physics
	public static final int PHYSICS_START_VY = -1500;
	public static final int PHYSICS_A = 1100;
	public static final int PHYSICS_MAX_VY = 1000;

	// Platforms
	public static final int PLATFORM_DY = 600, PLATFORM_DEATH = 500;
	public static final int PLATFORM_BOUNCE_SPEED_DEFAULT = -1200;
	public static final int PLATFORM_BOUNCE_SPEED_BOUNCE = -1200 * 3;
	public static final int PLATFORM_MAX_MOVESPEED = (int) (PLAYER_MAX_VX * .5);
	public static final Dimension PLATFORM_SIZE = new Dimension(128, 32);

	// Enemies
	public static final int ENEMY_BOUNCE_SPEED = -3000;
	public static final double ENEMY_PROBABILITY = .1;

	// Projectiles
	public static Map<ProjectileType, Integer> PROJECTILE_SPEED;

	// Camera
	public static final double OFFSET_X = 0;
	public static final double OFFSET_Y = 0;

	// Game
	public static final int DEATH_DISTANCE = 200;
	public static final int DEATH_COOLDOWN = 1000;
	public static final int GAME_INTERVAL = 20;

	public static void init()
	{
		PROJECTILE_SPEED = new HashMap<>();

		PROJECTILE_SPEED.put(ProjectileType.BALL, 1000);
		PROJECTILE_SPEED.put(ProjectileType.PEA, 2000);
		PROJECTILE_SPEED.put(ProjectileType.LASER, 3000);
	}
}