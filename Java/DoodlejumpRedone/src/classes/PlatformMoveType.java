package classes;

public enum PlatformMoveType
{
	STILL, HORIZONTAL, VERTICAL
}