package classes;

public class Printer extends Thread
{
	long interval = 10;
	String message = "";

	@Override
	public void run()
	{
		long startTime = System.currentTimeMillis();
		int step = 0;

		if (message.length() == 0)
			return;

		while (true)
		{
			try
			{
				Thread.sleep(interval);
			} catch (InterruptedException ignored)
			{
			}

			System.out.print(message.charAt(step));
			step++;
			if (step >= message.length())
			{
				System.out.println();
				return;
			}
		}
	}

	public void print(long i, String a)
	{
		interval = i;
		message = a;
		start();
	}
}
