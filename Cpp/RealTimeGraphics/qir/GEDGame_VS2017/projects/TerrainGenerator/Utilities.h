#include <cmath>

#define M_PI 3.14159265358979323846

struct float3
{
	float x;
	float y;
	float z;

	// Below are 'constructors' for the struct
	float3() : x(0), y(0), z(0) {}
	float3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
	float3(const float3& v) : x(v.x), y(v.y), z(v.z) {}

	// Util functions
	float mag() { return sqrt(x*x + y * y + z * z); }
	float3 norm() { float r = 1 / mag(); return float3(x*r, y*r, z*r); }
	float3 cross(const float3& v)
	{
		return float3(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
	}

	// Operator Overloading
	float3 operator + (const float3& v) { return float3(x + v.x, y + v.y, z + v.z); }
	float3 operator - (const float3& v) { return float3(x - v.x, y - v.y, z - v.z); }
	float3 operator * (const float& s) { return float3(x * s, y * s, z * s); }
	float3 operator / (const float& s) { return float3(x / s, y / s, z / s); }
	float operator * (const float3& v) { return x * v.x + y * v.y + z * v.z; }

	float3 operator += (const float3& v) { x += v.x; y += v.y; z += v.z; return *this; }
	float3 operator -= (const float3& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }
	float3 operator *= (const float& s) { x *= s; y *= s; z *= s; return *this; }
	float3 operator /= (const float& s) { x /= s; y /= s; z /= s; return *this; }

	float& operator [] (std::size_t i) { return *((float*)this + i); }

	std::string toString()
	{
		std::stringstream ss;
		ss << "[ " << std::fixed << std::setprecision(2);
		ss << x << " | " << y << " | " << z << " ]";
		return ss.str();
	}
};

struct float4
{
	float x;
	float y;
	float z;
	float w;

	// Below are 'constructors' for the struct
	float4() : x(0), y(0), z(0), w(0) {}
	float4(float _x, float _y, float _z, float _w) : x(_x), y(_y), z(_z), w(_w) {}
	float4(const float4& v) : x(v.x), y(v.y), z(v.z), w(v.w) {}

	// Operator Overloading
	float4 operator + (const float4& v) { return float4(x + v.x, y + v.y, z + v.z, w + v.w); }
	float4 operator - (const float4& v) { return float4(x - v.x, y - v.y, z - v.z, w - v.w); }
	float4 operator * (const float& s) { return float4(x * s, y * s, z * s, w * s); }
	float4 operator / (const float& s) { return float4(x / s, y / s, z / s, w / s); }
	float operator * (const float4& v) { return x * v.x + y * v.y + z * v.z; }

	float4 operator += (const float4& v) { x += v.x; y += v.y; z += v.z; return *this; }
	float4 operator -= (const float4& v) { x -= v.x; y -= v.y; z -= v.z; return *this; }
	float4 operator *= (const float& s) { x *= s; y *= s; z *= s; w *= s; return *this; }
	float4 operator /= (const float& s) { x /= s; y /= s; z /= s; w /= s; return *this; }

	float& operator [] (std::size_t i) { return *((float*)this + i); }

	std::string toString()
	{
		std::stringstream ss;
		ss << "[ " << std::fixed << std::setprecision(2);
		ss << x << " | " << y << " | " << z << " | " << w << " ]";
		return ss.str();
	}
};

inline const unsigned index(const unsigned x, const unsigned y, const unsigned width)
{
	return x + y * width;
}