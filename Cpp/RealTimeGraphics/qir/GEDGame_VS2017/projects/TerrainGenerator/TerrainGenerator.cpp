#include <iostream>
#include <tchar.h>
#include <iomanip>
#include <windows.h>
#include <string>
#include <sstream>
#include <ctime>
#include <random>
#include <SimpleImage.h>
#include <TextureGenerator.h>
#include "Utilities.h"
#include "DiamondSquare.h"

#define PRINTOUT true
#define SHOWPROGRESS false

void smooth(std::vector<float> *heightMap, const unsigned resolution, const int radius)
{
	const unsigned length = (resolution + 1) * (resolution + 1);
	float *const smoothed_array = new float[length]();

	for (int y = 0; y < (resolution + 1); y++)
	{
		for (int x = 0; x < (resolution + 1); x++)
		{
			float sum = 0.0f;
			int valueamount = 0;

			for (int ys = y - radius; ys <= y + radius; ys++)
				for (int xs = x - radius; xs <= x + radius; xs++)
					if (xs >= 0 && ys >= 0 && xs < (resolution + 1) && ys < (resolution + 1))
					{
						sum += heightMap->at(xs + ys * (resolution + 1));
						valueamount++;
					}
			smoothed_array[x + y * (resolution + 1)] = sum / valueamount;
		}
	}

	for (int i = 0; i < length; i++)
		heightMap->at(i) = smoothed_array[i];

	delete[] smoothed_array;
}

void generate(std::vector<float> *heightMap, const unsigned resolution)
{
	int ssteps = 5;
	if (PRINTOUT)
	{
		std::cout << "Generating HeightMap... ";
		std::cout << "diamond square";
	}
	DiamondSquare::DS(heightMap, resolution, 0.5);
	if (PRINTOUT && SHOWPROGRESS) std::cout << "\b\b\b\b\b\b\b\b\b\b\b\b\b\bsmoothing(00%)";
	for (int i = 0; i < ssteps; i++)
	{
		smooth(heightMap, resolution, 1);
		float progress = (float)i / ssteps;
		int percent = (int)(progress * 100);
		if (PRINTOUT && SHOWPROGRESS)
		{
			std::cout << "\b\b\b\b";
			std::printf("%02d%%)", percent);
		}
	}
	if (PRINTOUT) std::cout << "\b\b\b\b\b\b\b\b\b\b\b\b\b\bDone!         " << std::endl;
}

void downSample(std::vector<float> *heightMap, std::vector<float> *heightMapScaled,
	const unsigned resolution, const unsigned factor)
{
	if (PRINTOUT) std::cout << "Downsampling........... ";
	for (int y = 0; y < resolution / factor; y++) for (int x = 0; x < resolution / factor; x++)
		heightMapScaled->push_back(heightMap->at(index(x * factor, y * factor, resolution + 1)));
	if (PRINTOUT) std::cout << "Done!" << std::endl;
}

void calcAlphas(float height, float slope, float& alpha1, float& alpha2, float& alpha3)
{
	float fac = 15.0f;

	alpha1 = 1.0f / (1.0f + pow(2.0f, -15.0f * ((1 - height) * slope - 0.37f)));
	alpha2 = height * .6;
	alpha3 = height * slope * .6;
}

void blendPixel(const float height, const float3 normal, std::vector<float3> *c, float3& colour)
{
	float slope = (1 - normal.z) * 2;
	float a1, a2, a3;
	calcAlphas(height, slope, a1, a2, a3);
	colour = c->at(3) * a3 + (c->at(2) * a2 + (c->at(1) * a1 + c->at(0) * (1 - a1)) * (1 - a2)) * (1 - a3);
}

void blendLayers(std::vector<float> *heightMap, std::vector<float3> *normalMap, std::vector<float3> *colorMap,
	const unsigned resolution, std::vector<GEDUtils::SimpleImage> *images)
{
	if (PRINTOUT) std::cout << "Texture Blending....... ";
	if (PRINTOUT && SHOWPROGRESS) std::cout << "00%";
	for (unsigned y = 0; y < resolution; y++) for (unsigned x = 0; x < resolution; x++)
	{
		std::vector<float3> colours;
		colours.push_back(float3());
		colours.push_back(float3());
		colours.push_back(float3());
		colours.push_back(float3());
		images->at(0).getPixel(x % images->at(0).getWidth(), y % images->at(0).getHeight(), colours.at(0).x, colours.at(0).y, colours.at(0).z);
		images->at(1).getPixel(x % images->at(1).getWidth(), y % images->at(1).getHeight(), colours.at(1).x, colours.at(1).y, colours.at(1).z);
		images->at(2).getPixel(x % images->at(2).getWidth(), y % images->at(2).getHeight(), colours.at(2).x, colours.at(2).y, colours.at(2).z);
		images->at(3).getPixel(x % images->at(3).getWidth(), y % images->at(3).getHeight(), colours.at(3).x, colours.at(3).y, colours.at(3).z);

		float3 colour;
		blendPixel(heightMap->at(index(x, y, resolution + 1)), normalMap->at(index(x, y, resolution)), &colours, colour);
		colorMap->push_back(colour);

		if (y % 20 == 0 && PRINTOUT && SHOWPROGRESS)
		{
			float progress = (float)y / resolution;
			int percent = (int)(progress * 100);
			std::cout << "\b\b\b";
			std::printf("%02d%%", percent);
		}
	}
	if (PRINTOUT && SHOWPROGRESS) std::cout << "\b\b\b";
	if (PRINTOUT) std::cout << "Done!" << std::endl;
}

void printToImage(GEDUtils::SimpleImage *image, std::vector<float> *heightmap, const unsigned resolution)
{
	for (unsigned x = 0; x < resolution; x++) for (unsigned y = 0; y < resolution; y++)
		image->setPixel(x, y, heightmap->at(index(x, y, resolution)));
}

int _tmain(int argc, _TCHAR* argv[])
{
	if (argc % 2 == 0 && PRINTOUT) std::cerr << "bad arguments" << std::endl;

	unsigned int resolution;

	std::wstring heightOut;
	std::wstring colorOut;
	std::wstring normalOut;

	for (int i = 1; i < argc; i += 2)
	{
		if (_tcscmp(argv[i], TEXT("-r")) == 0)
			resolution = _tstoi(argv[i + 1]);
		else if (_tcscmp(argv[i], TEXT("-o_height")) == 0)
			heightOut = argv[i + 1];
		else if (_tcscmp(argv[i], TEXT("-o_color")) == 0)
			colorOut = argv[i + 1];
		else if (_tcscmp(argv[i], TEXT("-o_normal")) == 0)
			normalOut = argv[i + 1];
	}

	if (PRINTOUT)
	{
		if (resolution > 0)
			std::wcout << "resolution: " << resolution << std::endl;
		else
			std::wcerr << "resolution: " << resolution << " BAD!" << std::endl;
		std::wcout << "heightOut:  " << heightOut << std::endl;
		std::wcout << "colorOut:   " << colorOut << std::endl;
		std::wcout << "normalOut:  " << normalOut << std::endl;
		std::cout << std::endl;
	}

	std::vector<float> heightMap;
	heightMap.reserve((resolution + 1) * (resolution + 1));
	generate(&heightMap, resolution);

	// Downsample heightfield
	const unsigned fac = 4;

	std::vector<float> heightMapScaled;
	heightMapScaled.reserve((resolution / fac) * (resolution / fac));
	downSample(&heightMap, &heightMapScaled, resolution, fac);

	GEDUtils::SimpleImage heightImage(resolution / fac, resolution / fac);
	printToImage(&heightImage, &heightMapScaled, resolution / fac);
	try {
		heightImage.save(heightOut.c_str());
	}
	catch (std::exception& e) {
		if (PRINTOUT) std::cout << e.what();
	}

	// Generating Normals:

	std::vector<float3> normalMap;
	float3 uV; // x = u
	float3 vV; // y = v
	float3 tempV;
	normalMap.reserve(resolution * resolution);

	if (PRINTOUT) std::cout << "Calculating Normals.... ";
	if (PRINTOUT && SHOWPROGRESS) std::cout << "00%";
	for (int y = 0; y < resolution; ++y) {
		for (int x = 0; x < resolution; ++x) {
			// x = u
			uV.x = 1;
			uV.y = 0;
			uV.z = heightMap[index(x + 1, y, resolution + 1)] - heightMap[index((x == 0) ? x : (x - 1), y, resolution + 1)];
			if (x != 0)
				uV.z /= 2.0;
			uV.z *= resolution;

			// y = v
			vV.x = 0;
			vV.y = 1;
			vV.z = heightMap[index(x, y + 1, resolution + 1)] - heightMap[index(x, (y == 0) ? y : (y - 1), resolution + 1)];
			if (y != 0)
				vV.z /= 2.0;
			vV.z *= resolution;

			tempV = uV.cross(vV);
			tempV = tempV.norm();
			normalMap.push_back(tempV);

			if (y % 50 == 0 && PRINTOUT && SHOWPROGRESS)
			{
				float progress = (float)y / resolution;
				int percent = (int)(progress * 100);
				std::cout << "\b\b\b";
				std::printf("%02d%%", percent);
			}
		}
	}
	if (PRINTOUT && SHOWPROGRESS) std::cout << "\b\b\b";
	if (PRINTOUT) std::cout << "Done!" << std::endl;

	GEDUtils::SimpleImage normalImage(resolution, resolution);

	for (unsigned x = 0; x < resolution; ++x) {
		for (unsigned y = 0; y < resolution; ++y) {
			tempV = normalMap.at(index(x, y, resolution));
			normalImage.setPixel(x, y, ((tempV.x + 1) / 2.0), (tempV.y + 1) / 2.0, (tempV.z + 1) / 2.0); //Normal values have to be scaled to [0, 1]
		}
	}

	try {
		normalImage.save(normalOut.c_str());
	}
	catch (std::exception& e) {
		if (PRINTOUT) std::wcout << e.what();
	}

	// Generating Colors:

	std::vector<GEDUtils::SimpleImage> images;
	images.push_back(GEDUtils::SimpleImage("../../../../external/textures/gras15.jpg"));
	images.push_back(GEDUtils::SimpleImage("../../../../external/textures/Holz-34.jpg"));
	images.push_back(GEDUtils::SimpleImage("../../../../external/textures/debug_red.jpg"));
	images.push_back(GEDUtils::SimpleImage("../../../../external/textures/debug_yellow.jpg"));

	std::vector<float3> colorMap;
	colorMap.reserve(resolution * resolution);
	blendLayers(&heightMap, &normalMap, &colorMap, resolution, &images);

	GEDUtils::SimpleImage colorImage(resolution, resolution);
	for (unsigned y = 0; y < resolution; y++) for (unsigned x = 0; x < resolution; x++)
		colorImage.setPixel(x, y,
			colorMap.at(index(x, y, resolution)).x,
			colorMap.at(index(x, y, resolution)).y,
			colorMap.at(index(x, y, resolution)).z);

	try {
		colorImage.save(colorOut.c_str());
	}
	catch (std::exception& e) {
		if (PRINTOUT) std::wcout << e.what();
	}
	/*
	// Datei Generierung durch angegebene Tools
	std::cout << "TextureGenerator....... ";
	GEDUtils::TextureGenerator generator(TEXT("../../../../external/textures/gras15.jpg"),
		TEXT("../../../../external/textures/holz-34.jpg"),
		TEXT("../../../../external/textures/debug_red.jpg"),
		TEXT("../../../../external/textures/debug_yellow.jpg"));

	generator.generateAndStoreImages(heightMap, resolution, TEXT("color_old.tiff"), TEXT("normal_old.tiff"));
	std::cout << "Done!" << std::endl;
	*/
	return 0;
}