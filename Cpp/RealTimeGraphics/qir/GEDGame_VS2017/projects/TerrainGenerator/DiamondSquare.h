#include <iostream>
#include <vector>
#include <string>
#include <random>

class DiamondSquare
{
private:
	static void Diamond(std::vector<float> *heightMap, const unsigned resolution,
		const int x, const int y, const int s,
		const unsigned i, const float roughness, float random)
	{
		if (x == resolution)
		{
			heightMap->at(XYtoIndex(x, y, resolution + 1)) = heightMap->at(XYtoIndex(0, y, resolution + 1));
			return;
		}
		if (y == resolution)
		{
			heightMap->at(XYtoIndex(x, y, resolution + 1)) = heightMap->at(XYtoIndex(x, 0, resolution + 1));
			return;
		}
		float avg = 0;
		int amount = 0;
		for (int yo = -1; yo <= 1; yo += 2)
		{
			for (int xo = -1; xo <= 1; xo += 2)
			{
				int xn = x + xo * s, yn = y + yo * s;
				if (xn >= 0 && xn <= resolution && yn >= 0 && yn <= resolution)
				{
					avg += heightMap->at(XYtoIndex(xn, yn, resolution + 1));
					amount++;
				}
			}
		}
		avg /= amount;
		random *= pow(roughness, i);
		heightMap->at(XYtoIndex(x, y, resolution + 1)) = avg + random;
	}

	static void Square(std::vector<float> *heightMap, const unsigned resolution,
		const int x, const int y, const int s,
		const unsigned i, const float roughness, float random)
	{
		if (x < 0 || x > resolution || y < 0 || y > resolution) return;

		if (x == resolution)
		{
			heightMap->at(XYtoIndex(x, y, resolution + 1)) = heightMap->at(XYtoIndex(0, y, resolution + 1));
			return;
		}
		if (y == resolution)
		{
			heightMap->at(XYtoIndex(x, y, resolution + 1)) = heightMap->at(XYtoIndex(x, 0, resolution + 1));
			return;
		}

		float avg = 0;
		int amount = 0;
		for (unsigned i2 = 0; i2 < 4; i2++)
		{
			int xo = 0, yo = 0;
			switch (i2)
			{
			case 0:
				xo = s;
				break;
			case 1:
				yo = s;
				break;
			case 2:
				xo = -s;
				break;
			case 3:
				yo = -s;
			}
			int xn = x + xo, yn = y + yo;
			if (xn >= 0 && xn <= resolution && yn >= 0 && yn <= resolution)
			{
				avg += heightMap->at(XYtoIndex(xn, yn, resolution + 1));
				amount++;
			}
		}
		if (amount == 0) return;
		avg /= amount;
		random *= pow(roughness, i);
		heightMap->at(XYtoIndex(x, y, resolution + 1)) = avg + random;
	}

	static void Corners(std::vector<float> *heightMap, const unsigned int resolution, const float roughness,
		std::default_random_engine generator, std::normal_distribution<double> distribution)
	{
		float temp = -1;
		do {
			temp = distribution(generator);
		} while (temp < -0.5f || temp > 0.5f);
		temp *= roughness;
		heightMap->at(0) = temp;
		temp = -1;
		do {
			temp = distribution(generator);
		} while (temp < -0.5f || temp > 0.5f);
		temp *= roughness;
		heightMap->at(resolution) = temp;
		temp = -1;
		do {
			temp = distribution(generator);
		} while (temp < -0.5f || temp > 0.5f);
		temp *= roughness;
		heightMap->at(resolution * (resolution + 1)) = temp;
		temp = -1;
		do {
			temp = distribution(generator);
		} while (temp < -0.5f || temp > 0.5f);
		temp *= roughness;
		heightMap->at((resolution + 1) * (resolution + 1) - 1) = temp;
	}

	static int XYtoIndex(const unsigned x, const unsigned y, const unsigned w) { return y * w + x; }

public:
	static void DS(std::vector<float> *heightMap, const unsigned resolution, const float roughness)
	{
		std::default_random_engine generator(7);
		std::normal_distribution<double> distribution(0, roughness);
		const unsigned length = (resolution + 1) * (resolution + 1);

		for (unsigned i = 0; i < length; i++)
			heightMap->push_back(0);

		Corners(heightMap, resolution, roughness, generator, distribution);

		float temp = -1;

		for (int s = (resolution + 1) / 2, i = 1; s >= 1; s /= 2, i++)
		{
			for (int y = s; y <= resolution; y += s * 2)
			{
				for (int x = s; x <= resolution; x += s * 2)
				{
					do {
						temp = distribution(generator);
					} while (temp < -0.5f || temp > 0.5f);
					Diamond(heightMap, resolution, x, y, s, i, roughness, temp);
				}
			}
			for (int y = s; y <= resolution + s; y += s * 2)
			{
				for (int x = s; x <= resolution + s; x += s * 2)
				{
					do {
						temp = distribution(generator);
					} while (temp < -0.5f || temp > 0.5f);
					Square(heightMap, resolution, x - s, y, s, i, roughness, temp);
					do {
						temp = distribution(generator);
					} while (temp < -0.5f || temp > 0.5f);
					Square(heightMap, resolution, x, y - s, s, i, roughness, temp);
				}
			}
		}

		float min = 1, max = 0;

		for (int i = 0; i < (resolution + 1) * (resolution + 1); i++)
		{
			if (heightMap->at(i) < min) min = heightMap->at(i);
			if (heightMap->at(i) > max) max = heightMap->at(i);
		}
		for (int i = 0; i < (resolution + 1) * (resolution + 1); i++)
			heightMap->at(i) = (heightMap->at(i) - min) / (max - min) / 3.0f;
	}
};

