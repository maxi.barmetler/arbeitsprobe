//--------------------------------------------------------------------------------------
// Shader resources
//--------------------------------------------------------------------------------------

Texture2D g_EmissionSimple;
int       g_AmountSimple;

//--------------------------------------------------------------------------------------
// Constant buffers
//--------------------------------------------------------------------------------------

cbuffer cbConstant
{
	float4  g_LightDir; // Object space
	int     g_TerrainRes; // Terrain resolution
	float4  g_GlowColor;
	static const float PI = 3.14159265f;
	static const float radius = .75;
	static const float lineWidth = .005;
	static const float barWidth = .03;
	static const float gap = .01;
	static const float maxAngle = .17;
	static const float3 color = float3(.8, .8, .8);
	static const float3 colorGat = float3(.8, .4, .4);
	static const float3 colorPla = float3(.3, .5, .8);
};

cbuffer cbChangesEveryFrame
{
	matrix  g_CameraWorld;
	matrix  g_ViewProjection;
	float   g_Time;
	float4  g_CameraPosWorld;
	float4  g_CameraUp;
	float4  g_CameraRight;
	float2  g_BarValues;
	bool2   g_BarColored;
};

cbuffer cbUserChanges
{
};


//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------

struct HudVertex
{
	float3 pos : POSITION;
	float size : SIZE;
};

struct PSVertex
{
	float4 pos : SV_POSITION;
	float2 tex : TEXCOORD;
};


//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	AddressU = Wrap;
	AddressV = Wrap;
};

SamplerState samLinearClamp
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};


//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------

RasterizerState rsDefault
{
};

RasterizerState rsCullFront
{
	CullMode = Front;
};

RasterizerState rsCullBack
{
	CullMode = Back;
};

RasterizerState rsCullNone
{
	CullMode = None;
};

RasterizerState rsLineAA
{
	CullMode = None;
	AntialiasedLineEnable = true;
};


//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------

DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
	DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

BlendState BSBlendOver
{
	BlendEnable[0] = TRUE;
	SrcBlend[0] = SRC_ALPHA;
	SrcBlendAlpha[0] = ONE;
	DestBlend[0] = INV_SRC_ALPHA;
	DestBlendAlpha[0] = INV_SRC_ALPHA;
};


//--------------------------------------------------------------------------------------
// Shaders
//--------------------------------------------------------------------------------------
HudVertex HudVS(HudVertex vertex)
{
	vertex.pos = mul(float4(vertex.pos, 1), g_CameraWorld);
	return vertex;
}

[maxvertexcount(4)]
void HudGS(point HudVertex vertex[1], inout TriangleStream<PSVertex> stream)
{
	float3 right = g_CameraRight * vertex[0].size;
	float3 up = g_CameraUp * vertex[0].size / 2;
	PSVertex v;

	v.pos = mul(float4(vertex[0].pos - right + up, 1), g_ViewProjection);
	v.tex = float2(0, 0);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos - right - up, 1), g_ViewProjection);
	v.tex = float2(0, 1);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos + right + up, 1), g_ViewProjection);
	v.tex = float2(1, 0);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos + right - up, 1), g_ViewProjection);
	v.tex = float2(1, 1);
	stream.Append(v);
}

float4 HudPS(PSVertex pos) : SV_Target // This REALLY needs to be stored in a texture to improve performance.
{
	float x = 2 * (pos.tex.x - .5);
	float y = 1 * (pos.tex.y - .5);
	float distance = sqrt(pow(x , 2) + pow(y , 2));

	float3 output = color;
	if (y == 0 && x == 0)
		float4(0, 0, 0, 0);

	float totalWidth = 2 * lineWidth + 2 * gap + barWidth;
	float distanceFromLine = abs(distance - radius);
	float angle = acos(normalize(float2(x, y)).y) / PI - .5;
	float angleAbs = abs(angle);
	float alpha = 0;

	float percent = angle / (2 * maxAngle) + 0.5;

	if (distanceFromLine <= totalWidth / 2 && angleAbs <= maxAngle)
	{
		if (angleAbs > maxAngle - .5 * lineWidth)
		{
			alpha = 1;
		}
		else
		{
			alpha = .1;
			output *= .5;
			if (x < 0 && percent > .8) output = colorGat;

			if (distanceFromLine >= totalWidth / 2 - lineWidth)
			{
				alpha = 1;
				output = color;
			}
			else if (angleAbs < maxAngle - 1.2 * lineWidth && distanceFromLine <= barWidth / 2)
			{
				if (x < 0) // Gatling
				{
					if (percent <= g_BarValues.x)
					{
						alpha = 1;
						output = g_BarColored.x ? colorGat : color;
					}
				}
				if (x > 0) // Plasma
				{
					if (percent <= g_BarValues.y)
					{
						alpha = 1;
						output = g_BarColored.y ? colorPla : color;
					}
				}
			}
		}
	}

	return float4(output, alpha * .8);
}


//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------
technique11 Render
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, HudVS()));
		SetGeometryShader(CompileShader(gs_4_0, HudGS()));
		SetPixelShader(CompileShader(ps_4_0, HudPS()));

		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(BSBlendOver, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
}