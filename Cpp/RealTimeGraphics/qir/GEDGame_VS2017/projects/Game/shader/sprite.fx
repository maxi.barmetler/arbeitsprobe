//--------------------------------------------------------------------------------------
// Shader resources
//--------------------------------------------------------------------------------------

Texture2DArray g_Animation;

Texture2DArray g_EmissionGatlingBullet;
Texture2DArray g_EmissionGatlingFire;
Texture2DArray g_EmissionPlasma;
Texture2DArray g_EmissionExplosionA;
Texture2DArray g_EmissionExplosionB;
Texture2DArray g_EmissionExplosionC;

//--------------------------------------------------------------------------------------
// Constant buffers
//--------------------------------------------------------------------------------------

cbuffer cbConstant
{
	float4  g_LightDir; // Object space
	int     g_TerrainRes; // Terrain resolution
	float4  g_GlowColor;
};

cbuffer cbChangesEveryFrame
{
	matrix  g_CameraWorld;
	matrix  g_ViewProjection;
	float   g_Time;
	float4  g_CameraPosWorld;
	float4  g_CameraUp;
	float4  g_CameraRight;
};

cbuffer cbUserChanges
{
};


//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------

struct SpriteVertex
{
	float3 pos : POSITION;
	float2 radius : RADIUS;
	float progress : PROGRESS;
	float alpha : ALPHA;
	uint anim : ANIM;
	uint index : INDEX;
	uint parent : PARENT;
};

struct PSVertex
{
	float4 pos : SV_POSITION;
	float2 tex : TEXCOORD;
	float progress : PROGRESS;
	float alpha : ALPHA;
	uint anim : ANIM;
	uint index : INDEX;
};


//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	AddressU = Wrap;
	AddressV = Wrap;
};

SamplerState samLinearClamp
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};


//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------

RasterizerState rsDefault
{
};

RasterizerState rsCullFront
{
	CullMode = Front;
};

RasterizerState rsCullBack
{
	CullMode = Back;
};

RasterizerState rsCullNone
{
	CullMode = None;
};

RasterizerState rsLineAA
{
	CullMode = None;
	AntialiasedLineEnable = true;
};


//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------

DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
	DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

BlendState BSBlendOver
{
	BlendEnable[0] = TRUE;
	SrcBlend[0] = SRC_ALPHA;
	SrcBlendAlpha[0] = ONE;
	DestBlend[0] = INV_SRC_ALPHA;
	DestBlendAlpha[0] = INV_SRC_ALPHA;
};


//--------------------------------------------------------------------------------------
// Shaders
//--------------------------------------------------------------------------------------
SpriteVertex SpriteVS(SpriteVertex vertex)
{
	switch (vertex.parent)
	{
	case 0:
		break;
	case 1:
		vertex.pos = mul(float4(vertex.pos, 1), g_CameraWorld);
		break;
	}
	return vertex;
}

float4 SpritePS(PSVertex pos) : SV_Target
{
	pos.progress = min(pos.progress, 0.9999);

	float4 matEmission = float4(1, 0, 1, 1);
	float4 animCol = g_Animation.Sample(samAnisotropic, float3(pos.progress, 0, pos.anim));

	float3 dims; //(x-width, y-height, arraysize)
	switch (pos.index)
	{
	case 0:
		g_EmissionGatlingBullet.GetDimensions(dims.x, dims.y, dims.z); //dims is written here!
		matEmission = g_EmissionGatlingBullet.Sample(samAnisotropic, float3(pos.tex, (int) (pos.progress * dims.z)));
		break;

	case 1:
		g_EmissionGatlingFire.GetDimensions(dims.x, dims.y, dims.z); //dims is written here!
		matEmission = g_EmissionGatlingFire.Sample(samAnisotropic, float3(pos.tex, (int)(pos.progress * dims.z)));
		break;

	case 2:
		g_EmissionPlasma.GetDimensions(dims.x, dims.y, dims.z); //dims is written here!
		matEmission = g_EmissionPlasma.Sample(samAnisotropic, float3(pos.tex, (int)(pos.progress * dims.z)));
		break;

	case 3:
		g_EmissionExplosionA.GetDimensions(dims.x, dims.y, dims.z); //dims is written here!
		matEmission = g_EmissionExplosionA.Sample(samAnisotropic, float3(pos.tex, (int)(pos.progress * dims.z)));
		break;

	case 4:
		g_EmissionExplosionB.GetDimensions(dims.x, dims.y, dims.z); //dims is written here!
		matEmission = g_EmissionExplosionB.Sample(samAnisotropic, float3(pos.tex, (int)(pos.progress * dims.z)));
		break;

	case 5:
		g_EmissionExplosionC.GetDimensions(dims.x, dims.y, dims.z); //dims is written here!
		matEmission = g_EmissionExplosionC.Sample(samAnisotropic, float3(pos.tex, (int)(pos.progress * dims.z)));
		break;
	}
	matEmission.w *= pos.alpha;
	
	matEmission.x *= animCol.x;
	matEmission.y *= animCol.y;
	matEmission.z *= animCol.z;
	matEmission.w *= animCol.w;

	return matEmission;
}

[maxvertexcount(4)]
void SpriteGS(point SpriteVertex vertex[1], inout TriangleStream<PSVertex> stream)
{
	float3 right = g_CameraRight * vertex[0].radius.x;
	float3 up = g_CameraUp * vertex[0].radius.y;
	PSVertex v;
	v.index = vertex[0].index;
	v.progress = vertex[0].progress;
	v.alpha = vertex[0].alpha;
	v.anim = vertex[0].anim;

	v.pos = mul(float4(vertex[0].pos - right + up, 1), g_ViewProjection);
	v.tex = float2(0, 0);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos - right - up, 1), g_ViewProjection);
	v.tex = float2(0, 1);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos + right + up, 1), g_ViewProjection);
	v.tex = float2(1, 0);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos + right - up, 1), g_ViewProjection);
	v.tex = float2(1, 1);
	stream.Append(v);
}

[maxvertexcount(4)]
void SpriteGSVert(point SpriteVertex vertex[1], inout TriangleStream<PSVertex> stream)
{
	float3 right = g_CameraRight * vertex[0].radius.x;
	float3 up = float3(0, 1, 0) * vertex[0].radius.y * 2;
	PSVertex v;
	v.index = vertex[0].index;
	v.progress = vertex[0].progress;
	v.alpha = vertex[0].alpha;
	v.anim = vertex[0].anim;

	v.pos = mul(float4(vertex[0].pos - right + up, 1), g_ViewProjection);
	v.tex = float2(0, 0);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos - right, 1), g_ViewProjection);
	v.tex = float2(0, 1);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos + right + up, 1), g_ViewProjection);
	v.tex = float2(1, 0);
	stream.Append(v);
	v.pos = mul(float4(vertex[0].pos + right, 1), g_ViewProjection);
	v.tex = float2(1, 1);
	stream.Append(v);
}


//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------
technique11 Render
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, SpriteVS()));
		SetGeometryShader(CompileShader(gs_4_0, SpriteGS()));
		SetPixelShader(CompileShader(ps_4_0, SpritePS()));

		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(BSBlendOver, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}

	pass P1_NameTag
	{
		SetVertexShader(CompileShader(vs_4_0, SpriteVS()));
		SetGeometryShader(CompileShader(gs_4_0, SpriteGSVert()));
		SetPixelShader(CompileShader(ps_4_0, SpritePS()));

		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(BSBlendOver, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
}