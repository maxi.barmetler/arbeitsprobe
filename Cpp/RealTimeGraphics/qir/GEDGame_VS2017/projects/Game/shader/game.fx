//--------------------------------------------------------------------------------------
// Shader resources
//--------------------------------------------------------------------------------------

Texture2D      g_Diffuse;   // Material albedo for diffuse lighting
Texture2D      g_Specular;  // Material specular map
Texture2D      g_Glow;      // Material glow map
Texture2D      g_NormalMap; // Normal Map
Buffer<float>  g_HeightMap; // Height Map

//--------------------------------------------------------------------------------------
// Constant buffers
//--------------------------------------------------------------------------------------

cbuffer cbConstant
{
	float4  g_LightDir; // Object space
	int     g_TerrainRes; // Terrain resolution
	float4  g_GlowColor;
};

cbuffer cbChangesEveryFrame
{
	matrix  g_World;
	matrix  g_WorldViewProjection;
	float   g_Time;
	matrix	g_WorldNormals;
	float4  g_CameraPosWorld;
};

cbuffer cbUserChanges
{
};


//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------

struct PosNorTex
{
	float4 Pos : SV_POSITION;
	float4 Nor : NORMAL;
	float2 Tex : TEXCOORD;
};

struct PosTexLi
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
	float   Li : LIGHT_INTENSITY;
	float3 normal: NORMAL;
};

struct PosTex
{
	float4 Pos : SV_POSITION;
	float2 Tex : TEXCOORD;
};


struct T3dVertexVSIn {
	float3 Pos : POSITION; //Position in object space
	float2 Tex : TEXCOORD; //Texture coordinate
	float3 Nor : NORMAL;   //Normal in object space
	float3 Tan : TANGENT;  //Tangent in object space (not used in Ass. 5)
};

struct T3dVertexPSIn {
	float4 Pos : SV_POSITION;   //Position in clip space
	float2 Tex : TEXCOORD;      //Texture coordinate
	float3 PosWorld : POSITION; //Position in world space
	float3 NorWorld : NORMAL;   //Normal in world space
	float3 TanWorld : TANGENT;  //Tangent in world space (not used in Ass. 5)
};


//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState samAnisotropic
{
	Filter = ANISOTROPIC;
	AddressU = Wrap;
	AddressV = Wrap;
};

SamplerState samLinearClamp
{
	Filter = MIN_MAG_MIP_LINEAR;
	AddressU = Clamp;
	AddressV = Clamp;
};


//--------------------------------------------------------------------------------------
// Rasterizer states
//--------------------------------------------------------------------------------------

RasterizerState rsDefault {
};

RasterizerState rsCullFront {
	CullMode = Front;
};

RasterizerState rsCullBack {
	CullMode = Back;
};

RasterizerState rsCullNone {
	CullMode = None;
};

RasterizerState rsLineAA {
	CullMode = None;
	AntialiasedLineEnable = true;
};


//--------------------------------------------------------------------------------------
// DepthStates
//--------------------------------------------------------------------------------------

DepthStencilState EnableDepth
{
	DepthEnable = TRUE;
	DepthWriteMask = ALL;
	DepthFunc = LESS_EQUAL;
};

BlendState NoBlending
{
	AlphaToCoverageEnable = FALSE;
	BlendEnable[0] = FALSE;
};

//--------------------------------------------------------------------------------------
// Lighting models
//--------------------------------------------------------------------------------------

float4 Phong(float c[4], float4 mat[3], float4 colLight, float4 colLightAmbient, float3 n, float3 l, float3 r, float3 v, float s)
{
	return c[0] * mat[0] * saturate(dot(n, l)) * colLight
		+ c[1] * mat[1] * pow(saturate(dot(r, v)), s) * colLight
		+ c[2] * mat[0] * colLightAmbient
		+ c[3] * mat[2];
}

//--------------------------------------------------------------------------------------
// Shaders
//--------------------------------------------------------------------------------------

PosTexLi SimpleVS(PosNorTex Input)
{
	PosTexLi output = (PosTexLi)0;

	// Transform position from object space to homogenious clip space
	output.Pos = mul(Input.Pos, g_WorldViewProjection);

	// Pass trough normal and texture coordinates
	output.Tex = Input.Tex;

	// Calculate light intensity
	output.normal = normalize(mul(Input.Nor, g_World).xyz); // Assume orthogonal world matrix
	output.Li = saturate(dot(output.normal, g_LightDir.xyz));

	return output;
}

float4 SimplePS(PosTexLi Input) : SV_Target0
{
	// Perform lighting in object space, so that we can use the input normal "as it is"
	//float4 matDiffuse = g_Diffuse.Sample(samAnisotropic, Input.Tex);
	float4 matDiffuse = g_Diffuse.Sample(samLinearClamp, Input.Tex);
	return float4(matDiffuse.rgb * Input.Li, 1);
	//return float4(Input.normal, 1);
}

// Terrain

PosTex TerrainVS(uint VertexID : SV_VertexID)
{
	PosTex output = (PosTex)0;

	int x = VertexID % g_TerrainRes;
	int z = VertexID / g_TerrainRes;

	float4 pos;

	pos.x = (float)x / (g_TerrainRes - 1) - 0.5;
	pos.y = g_HeightMap[VertexID];
	pos.z = (float)z / (g_TerrainRes - 1) - 0.5;
	pos.w = 1.0;

	output.Pos = mul(pos, g_WorldViewProjection);
	output.Tex.xy = float2((float)x / g_TerrainRes, (float)z / g_TerrainRes);

	return output;
}

float4 TerrainPS(PosTex Input) : SV_Target0
{
	float3 n = float3(0.0, 0.0, 0.0);
	n.xz = g_NormalMap.Sample(samAnisotropic, Input.Tex).xy * 2.0 - 1.0;
	n.y = sqrt(1.0 - n.x * n.x - n.z * n.z);

	n = mul(float4(n.x, n.y, n.z, 0.0), g_WorldNormals).xyz;
	n = normalize(n);

	float4 matDiffuse = g_Diffuse.Sample(samLinearClamp, Input.Tex);
	float i = saturate(dot(n, g_LightDir.xyz));

	i = 0.05 + 0.95 * i; // Ambient light

	return i * matDiffuse;
}

float4 TerrainLightPS(PosTex Input) : SV_Target0
{
	float3 n = float3(0.0, 0.0, 0.0);
	n.xz = g_NormalMap.Sample(samAnisotropic, Input.Tex).xy * 2.0 - 1.0;
	n.y = sqrt(1.0 - n.x * n.x - n.z * n.z);

	n = mul(float4(n.x, n.y, n.z, 0.0), g_WorldNormals).xyz;
	n = normalize(n);

	float i = saturate(dot(n, g_LightDir.xyz));

	i = 0.05 + 0.95 * i; // Ambient light

	return i * float4(1.0, 1.0, 1.0, 1.0);
}

float4 TerrainNormalPS(PosTex Input) : SV_Target0
{
	float4 n = float4(0.0, 0.0, 0.0, 1.0);
	n.xz = g_NormalMap.Sample(samAnisotropic, Input.Tex).xy * 2.0 - 1.0;
	n.y = sqrt(1.0 - n.x * n.x - n.z * n.z);

	return n;
}

float4 TerrainTexPS(PosTex Input) : SV_Target
{
	float4 v = float4(0.0, 0.0, 0.0, 1.0);
	v.xy = Input.Tex;
	return v;
}

// Mesh

T3dVertexPSIn MeshVS(T3dVertexVSIn Input)
{
	T3dVertexPSIn output;
	output.Pos = mul(float4(Input.Pos, 1), g_WorldViewProjection);
	output.Tex = Input.Tex;
	output.PosWorld = mul(float4(Input.Pos, 1), g_World);
	output.NorWorld = normalize(mul(float4(Input.Nor, 0), g_WorldNormals).xyz);
	output.TanWorld = normalize(mul(float4(Input.Tan, 0), g_World).xyz);

	return output;
}

float4 MeshPS(T3dVertexPSIn Input) : SV_Target
{
	// Material colors
	float4 matDiffuse = g_Diffuse.Sample(samAnisotropic, Input.Tex);
	float4 matSpecular = g_Specular.Sample(samAnisotropic, Input.Tex);
	float4 matGlow = g_Glow.Sample(samAnisotropic, Input.Tex);

	// Light colors
	float4 colLight = float4(1, 1, 1, 1);
	float4 colAmbient = float4(1, 1, 1, 1);

	// Vectors
	float3 n = normalize(Input.NorWorld);
	float3 l = g_LightDir.xyz;
	float3 r = reflect(-l, n);
	float3 v = normalize(g_CameraPosWorld - Input.PosWorld);

	float4 mat[3] = { matDiffuse, matSpecular, matGlow };
	float c[4] = { 0.5, 0.4, 0.1, 0.5 };

	// float c[4], float4 mat[3], float4 colLight, float4 colLightAmbient, float3 n, float3 l, float3 r, float3 v, float s
	return Phong(c, mat, colLight, colAmbient, n, l, r, v, 20.0);
	return matDiffuse;
}

float4 GlowPS(T3dVertexPSIn Input) : SV_Target
{
	return g_GlowColor;
}

//--------------------------------------------------------------------------------------
// Techniques
//--------------------------------------------------------------------------------------
technique11 Render
{
	pass P0
	{
		SetVertexShader(CompileShader(vs_4_0, TerrainVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, TerrainPS()));

		SetRasterizerState(rsCullNone);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}

	pass P1_Mesh
	{
		SetVertexShader(CompileShader(vs_4_0, MeshVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, MeshPS()));

		SetRasterizerState(rsCullBack);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}

	pass P2_MeshGlow
	{
		SetVertexShader(CompileShader(vs_4_0, MeshVS()));
		SetGeometryShader(NULL);
		SetPixelShader(CompileShader(ps_4_0, GlowPS()));

		SetRasterizerState(rsCullBack);
		SetDepthStencilState(EnableDepth, 0);
		SetBlendState(NoBlending, float4(0.0f, 0.0f, 0.0f, 0.0f), 0xFFFFFFFF);
	}
}
