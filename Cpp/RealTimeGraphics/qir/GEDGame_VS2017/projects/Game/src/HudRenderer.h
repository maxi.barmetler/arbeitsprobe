#pragma once

#include "DXUT.h"
#include "d3dx11effect.h"
#include "SDKmisc.h"
#include "DXUTcamera.h"

#include <DDSTextureLoader.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

struct HudVertex
{
	DirectX::XMFLOAT3 position;
	float size;
};

class HudRenderer
{
public:
	HudRenderer();
	~HudRenderer();

	// Load/reload the effect. Must be called once before create!
	HRESULT reloadShader(ID3D11Device* device);
	// Release the effect again.
	void releaseShader();

	// Create all required D3D resources (textures, buffers, ...).
	// reloadShader must be called first!
	HRESULT create(ID3D11Device* device);
	// Release D3D resources again.
	void destroy();

	// Render the given sprites. They must already be sorted into back-to-front order.
	void render(ID3D11DeviceContext* context, float targetValues[2], bool colored[2],
		const CFirstPersonCamera& camera, float fElapsedTime );

private:

	float m_pBars[2];

	ID3DX11Effect* m_pEffect;

	std::vector<HudVertex> vertices;
	ID3D11Buffer* m_pVertexBuffer;
	ID3D11InputLayout* m_pInputLayout;
};

