#include "SpriteRenderer.h"


SpriteRenderer::SpriteRenderer(const std::vector<std::wstring>& textureFilenames)
{
	m_textureFilenames = textureFilenames;
	m_spriteCountMax = 1024000;
	m_pEffect = nullptr;
	m_pInputLayout = nullptr;
	m_pVertexBuffer = nullptr;
}

SpriteRenderer::~SpriteRenderer()
{

}


HRESULT SpriteRenderer::reloadShader(ID3D11Device* device)
{
	HRESULT hr;
	WCHAR path[MAX_PATH];

	// Find and load the rendering effect
	V_RETURN(DXUTFindDXSDKMediaFileCch(path, MAX_PATH, L"shader\\sprite.fxo"));
	std::ifstream is(path, std::ios_base::binary);
	is.seekg(0, std::ios_base::end);
	std::streampos pos = is.tellg();
	is.seekg(0, std::ios_base::beg);
	std::vector<char> effectBuffer((unsigned int)pos);
	is.read(&effectBuffer[0], pos);
	is.close();
	V_RETURN(D3DX11CreateEffectFromMemory((const void*)&effectBuffer[0], effectBuffer.size(), 0, device, &m_pEffect));
	assert(m_pEffect->IsValid());

	return S_OK;
}

void SpriteRenderer::releaseShader()
{
	SAFE_RELEASE(m_pEffect);
}


HRESULT SpriteRenderer::create(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	D3D11_BUFFER_DESC bd;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.ByteWidth = m_spriteCountMax * sizeof(SpriteVertex);
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.StructureByteStride = sizeof(SpriteVertex);

	V(device->CreateBuffer(&bd, NULL, &m_pVertexBuffer));

	const D3D11_INPUT_ELEMENT_DESC layout[] = // http://msdn.microsoft.com/en-us/library/bb205117%28v=vs.85%29.aspx
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "RADIUS",   0, DXGI_FORMAT_R32G32_FLOAT,    0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "PROGRESS", 0, DXGI_FORMAT_R32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "ALPHA",    0, DXGI_FORMAT_R32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "ANIM",     0, DXGI_FORMAT_R32_UINT,        0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "INDEX",    0, DXGI_FORMAT_R32_UINT,        0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "PARENT",   0, DXGI_FORMAT_R32_UINT,        0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);

	// Create the input layout
	D3DX11_PASS_DESC pd;
	V_RETURN(m_pEffect->GetTechniqueByName("Render")->GetPassByName("P0")->GetDesc(&pd));
	V_RETURN(device->CreateInputLayout(layout, numElements, pd.pIAInputSignature,
		pd.IAInputSignatureSize, &m_pInputLayout));

	for (std::wstring name : m_textureFilenames)
	{
		ID3D11ShaderResourceView* srv;
		V(DirectX::CreateDDSTextureFromFile(device, (L"resources\\" + name).c_str(), nullptr, &srv));
		m_spriteSRV.push_back(srv);
	}

	return hr;
}

void SpriteRenderer::destroy()
{
	SAFE_RELEASE(m_pVertexBuffer);
	SAFE_RELEASE(m_pInputLayout);

	for (auto &srv : m_spriteSRV)
	{
		SAFE_RELEASE(srv);
	}
	m_spriteSRV.clear();
}

void SpriteRenderer::renderSprites(ID3D11DeviceContext* context, const std::vector<SpriteVertex>& sprites, const CFirstPersonCamera& camera)
{
	D3D11_BOX box;
	box.left = 0; box.right = sprites.size() * sizeof(SpriteVertex);
	box.top = 0; box.bottom = 1;
	box.front = 0; box.back = 1;
	context->UpdateSubresource(m_pVertexBuffer, 0, &box, &sprites[0], 0, 0);

	unsigned int strides[] = { sizeof(SpriteVertex) }, offsets[] = { 0 };
	context->IASetVertexBuffers(0, 1, &m_pVertexBuffer, strides, offsets);
	context->IASetInputLayout(m_pInputLayout);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	DirectX::XMMATRIX viewProj = camera.GetViewMatrix() * camera.GetProjMatrix();

	m_pEffect->GetVariableByName("g_CameraWorld")->AsMatrix()->SetMatrix((float*)&camera.GetWorldMatrix());
	m_pEffect->GetVariableByName("g_ViewProjection")->AsMatrix()->SetMatrix((float*)&viewProj);
	m_pEffect->GetVariableByName("g_CameraUp")->AsVector()->SetFloatVector((float*)&camera.GetWorldUp());
	m_pEffect->GetVariableByName("g_CameraRight")->AsVector()->SetFloatVector((float*)&camera.GetWorldRight());

	m_pEffect->GetVariableByName("g_Animation")->AsShaderResource()
		->SetResource(m_spriteSRV[0]);

	m_pEffect->GetVariableByName("g_EmissionGatlingBullet")->AsShaderResource()
		->SetResource(m_spriteSRV[1]);
	m_pEffect->GetVariableByName("g_EmissionGatlingFire")->AsShaderResource()
		->SetResource(m_spriteSRV[2]);
	m_pEffect->GetVariableByName("g_EmissionPlasma")->AsShaderResource()
		->SetResource(m_spriteSRV[3]);
	m_pEffect->GetVariableByName("g_EmissionExplosionA")->AsShaderResource()
		->SetResource(m_spriteSRV[4]);
	m_pEffect->GetVariableByName("g_EmissionExplosionB")->AsShaderResource()
		->SetResource(m_spriteSRV[5]);
	m_pEffect->GetVariableByName("g_EmissionExplosionC")->AsShaderResource()
		->SetResource(m_spriteSRV[6]);

	m_pEffect->GetTechniqueByName("Render")->GetPassByName("P0")->Apply(0, context);

	context->Draw(sprites.size(), 0);
}