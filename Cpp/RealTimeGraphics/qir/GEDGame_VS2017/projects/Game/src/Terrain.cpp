#include "Terrain.h"

#include "GameEffect.h"
#include <DDSTextureLoader.h>
#include <SimpleImage.h>
#include "DirectXTex.h"

// You can use this macro to access your height field
#define IDX(X,Y,WIDTH) ((X) + (Y) * (WIDTH))

Terrain::Terrain(void) :
	indexBuffer(nullptr),
	diffuseTexture(nullptr),
	diffuseTextureSRV(nullptr),
	debugSRV(nullptr)
{
}


Terrain::~Terrain(void)
{
}

HRESULT Terrain::create(ID3D11Device* device, ConfigParser cparser)
{
	HRESULT hr;

	// In our example, we load a debug texture
	V(DirectX::CreateDDSTextureFromFile(device, L"resources\\debug_green.dds", nullptr, &debugSRV));

	if (hr != S_OK) {
		MessageBoxA(NULL, "Could not load texture \"resources\\debug_green.dds\"", "Invalid texture", MB_ICONERROR | MB_OK);
		return hr;
	}

	// Parse the config file

	std::string pathH = "resources/";
	pathH.append(cparser.getTerrainPath(0));

	GEDUtils::SimpleImage heightMap(pathH.c_str());

	unsigned res = heightMap.getWidth();

	center = heightMap.getPixel(res / 2, res / 2);

	/*
	std::vector<float> vertices;
	vertices.reserve(res*res*10);

	for (unsigned i = 0; i < res*res; ++i) {
		unsigned x = i % res;
		unsigned y = i / res;
		//x
		vertices.push_back( (((float)x/(res-1))-0.5) * width);
		//y
		vertices.push_back(height * heightMap.getPixel(x, y));
		//z
		vertices.push_back((((float)y/(res-1)) - 0.5) * depth);
		//w
		vertices.push_back(1.0);

		//normals
		float slopeU = (x == 0 || x == res - 1) ? 1.0 : 0.5;
		if(x == 0)
			slopeU *= heightMap.getPixel(x + 1, y) - heightMap.getPixel(x, y);
		else if(x == res-1)
			slopeU *= heightMap.getPixel(x, y) - heightMap.getPixel(x - 1, y);
		else
			slopeU *= heightMap.getPixel(x + 1, y) - heightMap.getPixel(x - 1, y);
		slopeU *= res;

		float slopeV = (y == 0 || y == res - 1) ? 1.0 : 0.5;
		if (y == 0)
			slopeV *= heightMap.getPixel(x, y + 1) - heightMap.getPixel(x, y);
		else if (y == res - 1)
			slopeV *= heightMap.getPixel(x, y) - heightMap.getPixel(x, y - 1);
		else
			slopeV *= heightMap.getPixel(x, y + 1) - heightMap.getPixel(x, y - 1);
		slopeV *= res;

		float normalizer = sqrt(slopeU*slopeU + slopeV*slopeV + 1);

		vertices.push_back(-slopeU * normalizer); //n.x
		vertices.push_back(-slopeV * normalizer); //n.y
		vertices.push_back(normalizer); //n.z
		vertices.push_back(0.0); //n.w

		//Tex
		vertices.push_back((float)x / (float)(res - 1));
		vertices.push_back((float)y / (float)(res - 1));
	}

	*/


	// Create index buffer
	std::vector<unsigned> indices;
	indices.reserve(6 * (res - 1)*(res - 1));
	for (unsigned y = 0; y < (res - 1); ++y) {
		for (unsigned x = 0; x < (res - 1); ++x) {
			//triangle 1
			indices.push_back(IDX(x, y, res));
			indices.push_back(IDX(x + 1, y, res));
			indices.push_back(IDX(x, y + 1, res));
			//triangle 2
			indices.push_back(IDX(x, y + 1, res));
			indices.push_back(IDX(x + 1, y, res));
			indices.push_back(IDX(x + 1, y + 1, res));
		}
	}

	D3D11_SUBRESOURCE_DATA id;
	id.pSysMem = &indices[0];
	id.SysMemPitch = sizeof(unsigned); // Stride
	id.SysMemSlicePitch = 0;

	D3D11_BUFFER_DESC bd;
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.ByteWidth = sizeof(unsigned) * indices.size(); //The size in bytes of the triangle array
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.Usage = D3D11_USAGE_DEFAULT;

	V(device->CreateBuffer(&bd, &id, &indexBuffer));

	// Load color texture (color map)
	// TODO: Insert your code to load the color texture and create
	// the texture "diffuseTexture" as well as the shader resource view
	// "diffuseTextureSRV"
	std::string tmp = "resources\\";
	tmp.append(cparser.getTerrainPath(1));
	std::wstring wstr;
	wstr.assign(tmp.begin(), tmp.end());
	DirectX::CreateDDSTextureFromFile(device, wstr.c_str(), (ID3D11Resource**)&diffuseTexture, &diffuseTextureSRV);

	tmp = "resources\\";
	tmp.append(cparser.getTerrainPath(2));
	wstr.assign(tmp.begin(), tmp.end());
	DirectX::CreateDDSTextureFromFile(device, wstr.c_str(), (ID3D11Resource**)&normalMap, &normalMapSRV);

	std::vector<float> heights;
	heights.reserve(res*res);

	for (unsigned y = 0; y < res; y++) for (unsigned x = 0; x < res; x++)
		heights.push_back(heightMap.getPixel(x, y));

	ZeroMemory(&id, sizeof(id));
	id.pSysMem = &heights[0];
	id.SysMemPitch = sizeof(float);
	id.SysMemSlicePitch = 0;

	ZeroMemory(&bd, sizeof(bd));
	bd.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	bd.ByteWidth = sizeof(float) * heights.size();

	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.Usage = D3D11_USAGE_DEFAULT;

	V(device->CreateBuffer(&bd, &id, &heightBuffer));

	D3D11_SHADER_RESOURCE_VIEW_DESC hbSRVdesc;
	hbSRVdesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;
	hbSRVdesc.Format = DXGI_FORMAT_R32_FLOAT;
	hbSRVdesc.Buffer.FirstElement = 0;
	hbSRVdesc.Buffer.NumElements = res * res;

	device->CreateShaderResourceView(heightBuffer, &hbSRVdesc, &heightBufferSRV);

	return hr;
}


void Terrain::destroy()
{
	// SAFE_RELEASE(vertexBuffer);
	SAFE_RELEASE(debugSRV);
	SAFE_RELEASE(indexBuffer);
	SAFE_RELEASE(diffuseTexture);
	SAFE_RELEASE(diffuseTextureSRV);
	SAFE_RELEASE(heightBuffer);
	SAFE_RELEASE(heightBufferSRV);
	SAFE_RELEASE(normalMap);
	SAFE_RELEASE(normalMapSRV);
}


void Terrain::render(ID3D11DeviceContext* context, ID3DX11EffectPass* pass)
{
	HRESULT hr;

	// Bind the terrain vertex buffer to the input assembler stage 
	ID3D11Buffer* vbs[] = { nullptr };
	unsigned int strides[] = { 0 }, offsets[] = { 0, };
	context->IASetVertexBuffers(0, 1, vbs, strides, offsets);
	// TODO: Bind the terrain index buffer to the input assembler stage
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Tell the input assembler stage which primitive topology to use
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	//context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	// DONE: Bind the SRV of the terrain diffuse texture to the effect variable
	// (instead of the SRV of the debug texture)
	V(g_gameEffect.diffuseEV->SetResource(diffuseTextureSRV));

	// Bind SRV of height and normal to effect variable
	V(g_gameEffect.heightmap->SetResource(heightBufferSRV));
	V(g_gameEffect.normalmap->SetResource(normalMapSRV));

	// Load resolution
	std::string pathH = "resources/";
	pathH.append(cparser.getTerrainPath(0));
	GEDUtils::SimpleImage heightMap(pathH.c_str());
	unsigned res = heightMap.getWidth();

	// Set resolution
	V(g_gameEffect.resolution->SetInt(res));

	// Apply the rendering pass in order to submit the necessary render state changes to the device
	V(pass->Apply(0, context));

	// Draw
	context->DrawIndexed((res - 1)*(res - 1) * 6, 0, 0);
}