#include "HudRenderer.h"

HudRenderer::HudRenderer()
{
	m_pEffect = nullptr;
	m_pInputLayout = nullptr;
	m_pVertexBuffer = nullptr;
	m_pBars[0] = m_pBars[1] = 0;
}

HudRenderer::~HudRenderer()
{

}

HRESULT HudRenderer::reloadShader(ID3D11Device* device)
{
	HRESULT hr;
	WCHAR path[MAX_PATH];

	// Find and load the rendering effect
	V_RETURN(DXUTFindDXSDKMediaFileCch(path, MAX_PATH, L"shader\\hud.fxo"));
	std::ifstream is(path, std::ios_base::binary);
	is.seekg(0, std::ios_base::end);
	std::streampos pos = is.tellg();
	is.seekg(0, std::ios_base::beg);
	std::vector<char> effectBuffer((unsigned int)pos);
	is.read(&effectBuffer[0], pos);
	is.close();
	V_RETURN(D3DX11CreateEffectFromMemory((const void*)&effectBuffer[0], effectBuffer.size(), 0, device, &m_pEffect));
	assert(m_pEffect->IsValid());

	return S_OK;
}

void HudRenderer::releaseShader()
{
	SAFE_RELEASE(m_pEffect);
}


HRESULT HudRenderer::create(ID3D11Device* device)
{
	HRESULT hr = S_OK;

	vertices.push_back({ {0, 0, 2}, 1.5 });

	D3D11_BUFFER_DESC bd;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.ByteWidth = 1 * sizeof(HudVertex);
	bd.CPUAccessFlags = 0;
	bd.MiscFlags = 0;
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.StructureByteStride = sizeof(HudVertex);

	D3D11_SUBRESOURCE_DATA id;
	id.pSysMem = &vertices[0];
	id.SysMemPitch = sizeof(HudVertex);
	id.SysMemSlicePitch = 0;
	V(device->CreateBuffer(&bd, &id, &m_pVertexBuffer));

	const D3D11_INPUT_ELEMENT_DESC layout[] = // http://msdn.microsoft.com/en-us/library/bb205117%28v=vs.85%29.aspx
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "SIZE",     0, DXGI_FORMAT_R32_FLOAT,       0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);

	// Create the input layout
	D3DX11_PASS_DESC pd;
	V_RETURN(m_pEffect->GetTechniqueByName("Render")->GetPassByName("P0")->GetDesc(&pd));
	V_RETURN(device->CreateInputLayout(layout, numElements, pd.pIAInputSignature,
		pd.IAInputSignatureSize, &m_pInputLayout));


	return hr;
}

void HudRenderer::destroy()
{
	SAFE_RELEASE(m_pVertexBuffer);
	SAFE_RELEASE(m_pInputLayout);
}

void HudRenderer::render(ID3D11DeviceContext* context, float targetValues[2], bool colored[2],
	const CFirstPersonCamera& camera, float fElapsedTime)
{
	float bFac = std::min(10 * fElapsedTime, .99f);

	for (int i = 0; i < 2; i++)
		m_pBars[i] = bFac * targetValues[i] + (1 - bFac) * m_pBars[i];

	D3D11_BOX box;
	box.left = 0; box.right = 1 * sizeof(HudVertex);
	box.top = 0; box.bottom = 1;
	box.front = 0; box.back = 1;
	context->UpdateSubresource(m_pVertexBuffer, 0, &box, &vertices[0], 0, 0);

	unsigned int strides[] = { sizeof(HudVertex) }, offsets[] = { 0 };
	context->IASetVertexBuffers(0, 1, &m_pVertexBuffer, strides, offsets);
	context->IASetInputLayout(m_pInputLayout);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);

	DirectX::XMMATRIX viewProj = camera.GetViewMatrix() * camera.GetProjMatrix();

	m_pEffect->GetVariableByName("g_BarColored")->AsVector()->SetBoolVector((bool*)colored);
	m_pEffect->GetVariableByName("g_BarValues")->AsVector()->SetFloatVector((float*)&m_pBars);
	m_pEffect->GetVariableByName("g_CameraWorld")->AsMatrix()->SetMatrix((float*)&camera.GetWorldMatrix());
	m_pEffect->GetVariableByName("g_ViewProjection")->AsMatrix()->SetMatrix((float*)&viewProj);
	m_pEffect->GetVariableByName("g_CameraUp")->AsVector()->SetFloatVector((float*)&camera.GetWorldUp());
	m_pEffect->GetVariableByName("g_CameraRight")->AsVector()->SetFloatVector((float*)&camera.GetWorldRight());

	m_pEffect->GetTechniqueByName("Render")->GetPassByName("P0")->Apply(0, context);

	context->Draw(vertices.size(), 0);
}