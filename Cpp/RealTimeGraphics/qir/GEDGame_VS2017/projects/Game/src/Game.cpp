#include <stdio.h>
#include <tchar.h>
#include <algorithm>

#include <windows.h>
#include <d3d11.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdint>
#include <random>


#include "dxut.h"
#include "DXUTmisc.h"
#include "DXUTcamera.h"
#include "DXUTgui.h"
#include "DXUTsettingsDlg.h"
#include "SDKmisc.h"

#include "d3dx11effect.h"

#include "Terrain.h"
#include "GameEffect.h"
#include "SpriteRenderer.h"
#include "HudRenderer.h"
#include "ConfigParser.h"

#include "debug.h"

#include "Mesh.h"



// Help macros
#define DEG2RAD( a ) ( (a) * XM_PI / 180.f )

#define CLAMP( a ) ( max(0, min(1, a)) )

using namespace DirectX;

//--------------------------------------------------------------------------------------
// Global variables
//--------------------------------------------------------------------------------------

// Camera
struct CAMERAPARAMS
{
	float   fovy;
	float   aspect;
	float   nearPlane;
	float   farPlane;
}                                       g_cameraParams;
float                                   g_cameraMoveScaler = 500.f;
float                                   g_cameraRotateScaler = 0.01f;
CFirstPersonCamera                      g_camera;               // A first person camera

// User Interface
CDXUTDialogResourceManager              g_dialogResourceManager; // manager for shared resources of dialogs
CD3DSettingsDlg                         g_settingsDlg;          // Device settings dialog
CDXUTTextHelper*                        g_txtHelper = NULL;
CDXUTDialog                             g_hud;                  // dialog for standard controls
CDXUTDialog                             g_sampleUI;             // dialog for sample specific controls


bool                                    g_drawCockpit = true;
bool                                    g_cameraMovement = false;
bool                                    g_drawColorObjects = true;
XMMATRIX                                g_terrainWorld;         // object- to world-space transformation


// Scene information
XMVECTOR                                g_lightDir;
Terrain									g_terrain;

GameEffect								g_gameEffect;           // CPU part of Shader

SpriteRenderer*                         g_SpriteRenderer;       // Sprite Renderer
HudRenderer*                            g_HudRenderer;          // Hud Renderer

//--------------------------------------------------------------------------------------
// UI control IDs
//--------------------------------------------------------------------------------------
#define IDC_TOGGLEFULLSCREEN    1
#define IDC_TOGGLECOCKPIT       2
#define IDC_TOGGLEREF           3
#define IDC_CHANGEDEVICE        4
#define IDC_RELOAD_SHADERS		101

//--------------------------------------------------------------------------------------
// Forward declarations 
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing,
	void* pUserContext);
void CALLBACK OnKeyboard(UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext);
void CALLBACK OnGUIEvent(UINT nEvent, int nControlID, CDXUTControl* pControl, void* pUserContext);
void CALLBACK OnFrameMove(double fTime, float fElapsedTime, void* pUserContext);
bool CALLBACK ModifyDeviceSettings(DXUTDeviceSettings* pDeviceSettings, void* pUserContext);

bool CALLBACK IsD3D11DeviceAcceptable(const CD3D11EnumAdapterInfo *, UINT, const CD3D11EnumDeviceInfo *,
	DXGI_FORMAT, bool, void*);
HRESULT CALLBACK OnD3D11CreateDevice(ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc,
	void* pUserContext);
HRESULT CALLBACK OnD3D11ResizedSwapChain(ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
	const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext);
void CALLBACK OnD3D11ReleasingSwapChain(void* pUserContext);
void CALLBACK OnD3D11DestroyDevice(void* pUserContext);
void CALLBACK OnD3D11FrameRender(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime,
	float fElapsedTime, void* pUserContext);

void InitApp();
void DeinitApp();
void RenderText();

void ReleaseShader();
HRESULT ReloadShader(ID3D11Device* pd3dDevice);

ConfigParser cparser;

// Game mechanics
void EnemyBehavior(float fElapsedTime);
void GunBehavior(float fElapsedTime);
void ParticleBehavior(float fElapsedTime);
void SpawnEnemy();
void DespawnEnemies();

struct gm_spawnConstants_struct
{
	int gm_spawnRadius;
	int gm_despawnRadius;
	int gm_targetRadius;
}                                       gm_spawnConstants;

struct gm_variables
{
	float enemySpawnCooldown = -1000;
	bool spawnEnemies = true;

	int   gatling_speed_mode = 0; // 0 = idle, 1 = speedup, 2 = slowDown
	float gatling_angle = 0.0f;
	int   gatling_nextShot = 0;
	float gatling_speed = 0.0f;
	float gatling_speed_max = .5f; // Hertz
	float gatling_heat = 0.0f;
	float gatling_overHeatPercent = 0;
	bool  gatling_overHeat = false;

	float plasma_power = 1.0f;

	XMFLOAT3 windDir = { 0, 0, 0 };

	float explosion_size = 1;      // this will be multiplied with the size of the ship
	float explosion_particles = 20; // this will be multiplied with the size of the explosion
	float explosion_speed = 1;
}                                       gm;


// Enemies
struct Enemy
{
	c_EnemyType type;
	float position[3];
	float rotation[3];
	float targetRotation[3];
	float velocity[3];
	int hp;
	float smoothFac = 1;

	void move(float fElapsedTime)
	{
		float smoothFacT = min(fElapsedTime * smoothFac, 1.f);

		for (int i = 0; i < 3; i++) {
			position[i] += velocity[i] * fElapsedTime;
			rotation[i] = (1 - smoothFacT) * rotation[i] + smoothFacT * targetRotation[i];
			// rotation[i] = targetRotation[i];
		}
	}

	void pointForward()
	{
		pointInDirection(velocity);
	}

	void pointInDirection(float vec[3])
	{
		float len = 0;
		for (int i = 0; i < 3; i++) len += pow(vec[i], 2);
		len = sqrtf(len);
		targetRotation[2] = 0;
		targetRotation[1] = -atan2(vec[2], vec[0]);
		targetRotation[0] = asin(vec[1] / len);
	}
};
std::list<Enemy>                        gm_enemies;

// Meshes
std::map<std::string, Mesh*>            g_meshes;

// Particles

struct Particle
{
	XMFLOAT3 pos;
	XMFLOAT2 radius;
	float alpha;
	int textureIndex;
	int animType;
	int parent;
	float age;
	float ageSpeed;
	float damage;
	XMFLOAT3 vel;
	XMFLOAT3 acc;
	float friction;

	Particle(
		XMFLOAT3 _pos, XMFLOAT2 _radius, int _textureIndex, int _parent,
		float _ageSpeed, float _damage, XMFLOAT3 _vel = { 0, 0, 0 }, XMFLOAT3 _acc = { 0, 0, 0 })
		:
		pos(_pos), radius(_radius), alpha(1), textureIndex(_textureIndex),
		animType(0), parent(_parent), age(0), ageSpeed(_ageSpeed),
		damage(_damage), vel(_vel), acc(_acc), friction(0)
	{ }

	Particle() :
		pos({ 0, 0, 0 }), radius({ 1, 1 }), alpha(1), textureIndex(0),
		animType(0), parent(0), age(0), ageSpeed(.00001),
		damage(0), vel({ 0, 0,0 }), acc({ 0, 0, 0 }), friction(0)
	{ }
};

std::list<Particle> particles;

//--------------------------------------------------------------------------------------
// Entry point to the program. Initializes everything and goes into a message processing 
// loop. Idle time is used to render the scene.
//--------------------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{

	UNREFERENCED_PARAMETER(argc);
	UNREFERENCED_PARAMETER(argv);

	// Enable run-time memory check for debug builds.
#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
#endif

	// Old Direct3D Documentation:
	// Start > All Programs > Microsoft DirectX SDK (June 2010) > Windows DirectX Graphics Documentation

	// DXUT Documentaion:
	// Start > All Programs > Microsoft DirectX SDK (June 2010) > DirectX Documentation for C++ : The DirectX Software Development Kit > Programming Guide > DXUT

	// New Direct3D Documentaion (just for reference, use old documentation to find explanations):
	// http://msdn.microsoft.com/en-us/library/windows/desktop/hh309466%28v=vs.85%29.aspx

	// Set DXUT callbacks
	DXUTSetCallbackMsgProc(MsgProc);
	DXUTSetCallbackKeyboard(OnKeyboard);
	DXUTSetCallbackFrameMove(OnFrameMove);
	DXUTSetCallbackDeviceChanging(ModifyDeviceSettings);

	DXUTSetCallbackD3D11DeviceAcceptable(IsD3D11DeviceAcceptable);
	DXUTSetCallbackD3D11DeviceCreated(OnD3D11CreateDevice);
	DXUTSetCallbackD3D11SwapChainResized(OnD3D11ResizedSwapChain);
	DXUTSetCallbackD3D11SwapChainReleasing(OnD3D11ReleasingSwapChain);
	DXUTSetCallbackD3D11DeviceDestroyed(OnD3D11DestroyDevice);
	DXUTSetCallbackD3D11FrameRender(OnD3D11FrameRender);
	//DXUTSetIsInGammaCorrectMode(false);

	InitApp();
	DXUTInit(true, true, NULL); // Parse the command line, show msgboxes on error, no extra command line params
	DXUTSetCursorSettings(false, true);
	DXUTCreateWindow(L"Yeet"); // You may change the title

	DXUTCreateDevice(D3D_FEATURE_LEVEL_10_0, true, 1280, 720);

	DXUTMainLoop(); // Enter into the DXUT render loop
	DXUTShutdown();
	DeinitApp();
	return DXUTGetExitCode();
}

//--------------------------------------------------------------------------------------
// Initialize the app 
//--------------------------------------------------------------------------------------
void InitApp()
{
	HRESULT hr;
	WCHAR path[MAX_PATH];

	// Parse the config file
	V(DXUTFindDXSDKMediaFileCch(path, MAX_PATH, L"game.cfg"));
	char pathA[MAX_PATH];
	size_t size;
	wcstombs_s(&size, pathA, path, MAX_PATH);
	cparser.load(pathA);

	// Intialize the user interface
	g_settingsDlg.Init(&g_dialogResourceManager);
	g_hud.Init(&g_dialogResourceManager);
	g_sampleUI.Init(&g_dialogResourceManager);

	g_hud.SetCallback(OnGUIEvent);
	int iY = 30;
	int iYo = 26;
	g_hud.AddButton(IDC_TOGGLEFULLSCREEN, L"Toggle full screen", 0, iY, 170, 22);
	g_hud.AddButton(IDC_TOGGLECOCKPIT, L"Toggle Cockpit (F1)", 0, iY += iYo, 170, 22, VK_F1);
	g_hud.AddButton(IDC_TOGGLEREF, L"Toggle REF (F3)", 0, iY += iYo, 170, 22, VK_F3);
	g_hud.AddButton(IDC_CHANGEDEVICE, L"Change device (F2)", 0, iY += iYo, 170, 22, VK_F2);

	g_hud.AddButton(IDC_RELOAD_SHADERS, L"Reload shaders (F5)", 0, iY += 24, 170, 22, VK_F5);

	g_sampleUI.SetCallback(OnGUIEvent); iY = 10;
	iY += 24;

	// Sprite Renderer
	g_SpriteRenderer = new SpriteRenderer(cparser.getCSpriteTextures());

	// Hud Renderer
	g_HudRenderer = new HudRenderer();

	for (std::string name : cparser.getMeshNames())
		g_meshes.insert(std::make_pair(name, new Mesh(cparser.getMeshPath(name, 0),
			cparser.getMeshPath(name, 1), cparser.getMeshPath(name, 2), cparser.getMeshPath(name, 3))));

	// Init Game mechanics
	float diagonal = sqrtf(pow(cparser.getTerrainWidth(), 2) + pow(cparser.getTerrainDepth(), 2));
	gm_spawnConstants =
	{
		(int)(diagonal),
		(int)(diagonal * 1.1f),
		(int)(diagonal * .1f)
	};
	gm.gatling_speed_max = 1.f / 6 / max(0.01, cparser.getCGun("Gatling").c_cooldown);

	g_camera.SetResetCursorAfterMove(true);



	particles.push_back(Particle({ -10, 100, 0 }, { 5, 5 }, 0, 0, .00001, 0));
	particles.push_back(Particle({ 10, 100, 0 }, { 5, 5 }, 0, 0, .00001, 0));
}

//--------------------------------------------------------------------------------------
// DeInitialize the app 
//--------------------------------------------------------------------------------------
void DeinitApp()
{
	for (auto m : g_meshes)
		SAFE_DELETE(m.second);

	SAFE_DELETE(g_SpriteRenderer);
	SAFE_DELETE(g_HudRenderer);
}

//--------------------------------------------------------------------------------------
// Render the help and statistics text. This function uses the ID3DXFont interface for 
// efficient text rendering.
//--------------------------------------------------------------------------------------
void RenderText()
{
	g_txtHelper->Begin();
	g_txtHelper->SetInsertionPos(5, 5);
	g_txtHelper->SetForegroundColor(XMVectorSet(1.0f, 1.0f, 0.0f, 1.0f));
	g_txtHelper->DrawTextLine(DXUTGetFrameStats(true)); //DXUTIsVsyncEnabled() ) );
	g_txtHelper->DrawTextLine(DXUTGetDeviceStats());
	g_txtHelper->End();
}

//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable(const CD3D11EnumAdapterInfo *, UINT, const CD3D11EnumDeviceInfo *,
	DXGI_FORMAT, bool, void*)
{
	return true;
}

//--------------------------------------------------------------------------------------
// Specify the initial device settings
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings(DXUTDeviceSettings* pDeviceSettings, void* pUserContext)
{
	UNREFERENCED_PARAMETER(pDeviceSettings);
	UNREFERENCED_PARAMETER(pUserContext);

	// For the first device created if its a REF device, optionally display a warning dialog box
	static bool s_bFirstTime = true;
	if (s_bFirstTime) {
		s_bFirstTime = false;
		if (pDeviceSettings->d3d11.DriverType == D3D_DRIVER_TYPE_REFERENCE)
			DXUTDisplaySwitchingToREFWarning();
	}
	// Enable anti aliasing
	pDeviceSettings->d3d11.sd.SampleDesc.Count = 4;
	pDeviceSettings->d3d11.sd.SampleDesc.Quality = 1;

	return true;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependant on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice(ID3D11Device* pd3dDevice,
	const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext)
{
	UNREFERENCED_PARAMETER(pBackBufferSurfaceDesc);
	UNREFERENCED_PARAMETER(pUserContext);

	HRESULT hr;

	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext(); // http://msdn.microsoft.com/en-us/library/ff476891%28v=vs.85%29
	V_RETURN(g_dialogResourceManager.OnD3D11CreateDevice(pd3dDevice, pd3dImmediateContext));
	V_RETURN(g_settingsDlg.OnD3D11CreateDevice(pd3dDevice));
	g_txtHelper = new CDXUTTextHelper(pd3dDevice, pd3dImmediateContext, &g_dialogResourceManager, 15);

	V_RETURN(ReloadShader(pd3dDevice));

	// Define the input layout
	const D3D11_INPUT_ELEMENT_DESC layout[] = // http://msdn.microsoft.com/en-us/library/bb205117%28v=vs.85%29.aspx
	{
		{ "SV_POSITION",    0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,  0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL",         0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TEXCOORD",       0, DXGI_FORMAT_R32G32_FLOAT,       0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};
	UINT numElements = sizeof(layout) / sizeof(layout[0]);

	// Create the input layout
	D3DX11_PASS_DESC pd;
	V_RETURN(g_gameEffect.pass0->GetDesc(&pd));
	// V_RETURN(pd3dDevice->CreateInputLayout(layout, numElements, pd.pIAInputSignature, pd.IAInputSignatureSize, &g_terrainVertexLayout));

	// Create the terrain
	V_RETURN(g_terrain.create(pd3dDevice, cparser));

	// Initialize the camera
	XMVECTOR vEye = XMVectorSet(0.0f, cparser.getTerrainHeigth() * g_terrain.center + 50, 0.0f, 0.0f);   // Camera eye is here
	XMVECTOR vAt = XMVectorSet(0.0f, XMVectorGetByIndex(vEye, 1), 1.0f, 1.0f);               // ... facing at this position
	g_camera.SetViewParams(vEye, vAt); // http://msdn.microsoft.com/en-us/library/windows/desktop/bb206342%28v=vs.85%29.aspx
	g_camera.SetScalers(g_cameraRotateScaler, g_cameraMoveScaler);

	for (auto p : g_meshes) {
		p.second->create(pd3dDevice);
	}

	Mesh::createInputLayout(pd3dDevice, g_gameEffect.meshPass1);

	V_RETURN(g_SpriteRenderer->create(pd3dDevice));
	V_RETURN(g_HudRenderer->create(pd3dDevice));

	return S_OK;
}


//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice(void* pUserContext)
{
	UNREFERENCED_PARAMETER(pUserContext);

	g_dialogResourceManager.OnD3D11DestroyDevice();
	g_settingsDlg.OnD3D11DestroyDevice();
	DXUTGetGlobalResourceCache().OnDestroyDevice();

	// Destroy the terrain
	g_terrain.destroy();

	std::cout << "Destroying meshes" << std::endl;

	for (auto p : g_meshes)
		p.second->destroy();

	Mesh::destroyInputLayout();

	g_SpriteRenderer->destroy();
	g_HudRenderer->destroy();

	SAFE_DELETE(g_txtHelper);
	ReleaseShader();
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain(ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
	const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext)
{
	UNREFERENCED_PARAMETER(pSwapChain);
	UNREFERENCED_PARAMETER(pUserContext);

	HRESULT hr;

	// Intialize the user interface

	V_RETURN(g_dialogResourceManager.OnD3D11ResizedSwapChain(pd3dDevice, pBackBufferSurfaceDesc));
	V_RETURN(g_settingsDlg.OnD3D11ResizedSwapChain(pd3dDevice, pBackBufferSurfaceDesc));

	g_hud.SetLocation(pBackBufferSurfaceDesc->Width - 170, 0);
	g_hud.SetSize(170, 170);
	g_sampleUI.SetLocation(pBackBufferSurfaceDesc->Width - 170, pBackBufferSurfaceDesc->Height - 300);
	g_sampleUI.SetSize(170, 300);

	// Initialize the camera

	g_cameraParams.aspect = pBackBufferSurfaceDesc->Width / (FLOAT)pBackBufferSurfaceDesc->Height;
	// g_cameraParams.fovy = 0.785398f;
	g_cameraParams.fovy = 0.785398f;
	g_cameraParams.nearPlane = 1.f;
	g_cameraParams.farPlane = 5000.f;

	g_camera.SetProjParams(g_cameraParams.fovy, g_cameraParams.aspect, g_cameraParams.nearPlane, g_cameraParams.farPlane);
	g_camera.SetEnablePositionMovement(false);
	g_camera.SetRotateButtons(false, false, false, true);
	g_camera.SetScalers(g_cameraRotateScaler, g_cameraMoveScaler);
	g_camera.SetDrag(true);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain(void* pUserContext)
{
	UNREFERENCED_PARAMETER(pUserContext);
	g_dialogResourceManager.OnD3D11ReleasingSwapChain();
}

//--------------------------------------------------------------------------------------
// Loads the effect from file
// and retrieves all dependent variables
//--------------------------------------------------------------------------------------
HRESULT ReloadShader(ID3D11Device* pd3dDevice)
{
	assert(pd3dDevice != NULL);

	HRESULT hr;

	ReleaseShader();
	V_RETURN(g_gameEffect.create(pd3dDevice));
	V_RETURN(g_SpriteRenderer->reloadShader(pd3dDevice));
	V_RETURN(g_HudRenderer->reloadShader(pd3dDevice));

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release resources created in ReloadShader
//--------------------------------------------------------------------------------------
void ReleaseShader()
{
	g_gameEffect.destroy();

	g_SpriteRenderer->releaseShader();
	g_HudRenderer->releaseShader();
}

//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam, bool* pbNoFurtherProcessing,
	void* pUserContext)
{
	UNREFERENCED_PARAMETER(pUserContext);

	// Pass messages to dialog resource manager calls so GUI state is updated correctly
	*pbNoFurtherProcessing = g_dialogResourceManager.MsgProc(hWnd, uMsg, wParam, lParam);
	if (*pbNoFurtherProcessing)
		return 0;

	// Pass messages to settings dialog if its active
	if (g_settingsDlg.IsActive()) {
		g_settingsDlg.MsgProc(hWnd, uMsg, wParam, lParam);
		return 0;
	}

	// Give the dialogs a chance to handle the message first
	*pbNoFurtherProcessing = g_hud.MsgProc(hWnd, uMsg, wParam, lParam);
	if (*pbNoFurtherProcessing)
		return 0;
	*pbNoFurtherProcessing = g_sampleUI.MsgProc(hWnd, uMsg, wParam, lParam);
	if (*pbNoFurtherProcessing)
		return 0;

	// Use the mouse weel to control the movement speed
	if (uMsg == WM_MOUSEWHEEL) {
		int zDelta = GET_WHEEL_DELTA_WPARAM(wParam);
		g_cameraMoveScaler *= (1 + zDelta / 500.0f);
		if (g_cameraMoveScaler < 0.1f)
			g_cameraMoveScaler = 0.1f;
		g_camera.SetScalers(g_cameraRotateScaler, g_cameraMoveScaler);
	}

	// Pass all remaining windows messages to camera so it can respond to user input
	g_camera.HandleMessages(hWnd, uMsg, wParam, lParam);

	return 0;
}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard(UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext)
{
	UNREFERENCED_PARAMETER(nChar);
	UNREFERENCED_PARAMETER(bKeyDown);
	UNREFERENCED_PARAMETER(bAltDown);
	UNREFERENCED_PARAMETER(pUserContext);

	//Enable position movement when the C-key is pressed
	if (nChar == 'C' && bKeyDown) g_cameraMovement ^= true;
	g_camera.SetEnablePositionMovement(g_cameraMovement);
}

//--------------------------------------------------------------------------------------
// Handles the GUI events
//--------------------------------------------------------------------------------------
void CALLBACK OnGUIEvent(UINT nEvent, int nControlID, CDXUTControl* pControl, void* pUserContext)
{
	UNREFERENCED_PARAMETER(nEvent);
	UNREFERENCED_PARAMETER(pControl);
	UNREFERENCED_PARAMETER(pUserContext);

	switch (nControlID) {
	case IDC_TOGGLEFULLSCREEN:
		DXUTToggleFullScreen();
		break;
	case IDC_TOGGLECOCKPIT:
		g_drawCockpit ^= true;
		break;
	case IDC_TOGGLEREF:
		DXUTToggleREF();
		break;
	case IDC_CHANGEDEVICE:
		g_settingsDlg.SetActive(!g_settingsDlg.IsActive());
		if (g_settingsDlg.IsActive()) {
			g_camera.SetRotateButtons(false, false, false, false);
			DXUTSetCursorSettings(true, true);
		}
		else {
			g_camera.SetRotateButtons(false, false, false, true);
			DXUTSetCursorSettings(false, true);
		}

		break;
	case IDC_RELOAD_SHADERS:
		ReloadShader(DXUTGetD3D11Device());
		break;
	}
}

//--------------------------------------------------------------------------------------
// Handle updates to the scene.  This is called regardless of which D3D API is used
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove(double fTime, float fElapsedTime, void* pUserContext)
{
	if (!g_settingsDlg.IsActive())
	{
		if (DXUTIsKeyDown(VK_LCONTROL)) {
			g_camera.SetRotateButtons(false, false, false, false);
		}
		else {
			g_camera.SetRotateButtons(false, false, false, true);
		}
	}

	UNREFERENCED_PARAMETER(pUserContext);
	// Update the camera's position based on user input 
	g_camera.FrameMove(fElapsedTime);

	// Initialize the terrain world matrix
	// http://msdn.microsoft.com/en-us/library/windows/desktop/bb206365%28v=vs.85%29.aspx

	// Start with identity matrix
	g_terrainWorld = DirectX::XMMatrixIdentity();

	// Scale terrain transform
	g_terrainWorld *= DirectX::XMMatrixScaling(cparser.getTerrainWidth(), cparser.getTerrainHeigth(), cparser.getTerrainDepth());

	// Set the light vector
	g_lightDir = XMVectorSet(1, 1, 1, 0); // Direction to the directional light in world space    
	g_lightDir = XMVector3Normalize(g_lightDir);

	EnemyBehavior(fElapsedTime);
	GunBehavior(fElapsedTime);
	ParticleBehavior(fElapsedTime);
}


//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender(ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext, double fTime,
	float fElapsedTime, void* pUserContext)
{
	UNREFERENCED_PARAMETER(pd3dDevice);
	UNREFERENCED_PARAMETER(fTime);
	UNREFERENCED_PARAMETER(pUserContext);

	HRESULT hr;

	// If the settings dialog is being shown, then render it instead of rendering the app's scene
	if (g_settingsDlg.IsActive()) {
		g_settingsDlg.OnRender(fElapsedTime);
		return;
	}

	ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	float clearColor[4] = { 0.5f, 0.5f, 0.5f, 1.0f };
	pd3dImmediateContext->ClearRenderTargetView(pRTV, clearColor);

	if (g_gameEffect.effect == NULL) {
		g_txtHelper->Begin();
		g_txtHelper->SetInsertionPos(5, 5);
		g_txtHelper->SetForegroundColor(XMVectorSet(1.0f, 1.0f, 0.0f, 1.0f));
		g_txtHelper->DrawTextLine(L"SHADER ERROR");
		g_txtHelper->End();
		return;
	}

	// Clear the depth stencil
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearDepthStencilView(pDSV, D3D11_CLEAR_DEPTH, 1.0, 0);

	// Update variables that change once per frame
	g_gameEffect.cameraPosWorldEV->SetFloatVector((float*)&g_camera.GetEyePt());

	XMMATRIX const view = g_camera.GetViewMatrix(); // http://msdn.microsoft.com/en-us/library/windows/desktop/bb206342%28v=vs.85%29.aspx
	XMMATRIX const proj = g_camera.GetProjMatrix(); // http://msdn.microsoft.com/en-us/library/windows/desktop/bb147302%28v=vs.85%29.aspx
	XMMATRIX worldViewProj = g_terrainWorld * view * proj;
	V(g_gameEffect.worldEV->SetMatrix((float*)&g_terrainWorld));
	V(g_gameEffect.worldViewProjectionEV->SetMatrix((float*)&worldViewProj));
	V(g_gameEffect.lightDirEV->SetFloatVector((float*)&g_lightDir));

	XMMATRIX inverseTransposedWorld = DirectX::XMMatrixTranspose(DirectX::XMMatrixInverse(nullptr, g_terrainWorld));
	V(g_gameEffect.worldNormalsEV->SetMatrix((float*)&inverseTransposedWorld));

	// Render ShadowMap (Big oof time)






	// ------------------

	// Set input layout
	pd3dImmediateContext->IASetInputLayout(nullptr);

	g_terrain.render(pd3dImmediateContext, g_gameEffect.pass0);

	// Render objects
	for (c_Object c : cparser.getCObjects()) {
		XMMATRIX g_world = DirectX::XMMatrixIdentity();
		switch (c.c_type) {
		case 0:
			g_world *= DirectX::XMMatrixScaling(c.c_scale, c.c_scale, c.c_scale);
			g_world *= DirectX::XMMatrixRotationY(c.c_rotation[1]);
			g_world *= DirectX::XMMatrixRotationZ(c.c_rotation[2]);
			g_world *= DirectX::XMMatrixRotationX(c.c_rotation[0]);
			g_world *= DirectX::XMMatrixTranslation(c.c_offset[0], c.c_offset[1], c.c_offset[2]);

			V(g_gameEffect.worldEV->SetMatrix((float*)&g_world));

			worldViewProj = g_world * view * proj;
			V(g_gameEffect.worldViewProjectionEV->SetMatrix((float*)&worldViewProj));

			inverseTransposedWorld = DirectX::XMMatrixTranspose(DirectX::XMMatrixInverse(nullptr, g_world));
			V(g_gameEffect.worldNormalsEV->SetMatrix((float*)&inverseTransposedWorld));

			g_meshes.at(c.c_name)->render(pd3dImmediateContext, g_gameEffect.meshPass1,
				g_gameEffect.diffuseEV, g_gameEffect.specularEV, g_gameEffect.glowEV);
			break;

		case 1:
			if (!g_drawCockpit)
				break;
			g_world *= DirectX::XMMatrixRotationY(c.c_rotation[1]);
			g_world *= DirectX::XMMatrixRotationZ(c.c_rotation[2]);
			g_world *= DirectX::XMMatrixRotationX(c.c_rotation[0]);
			if (c.c_name == "GatlingTop")
				g_world *= DirectX::XMMatrixRotationZ(-gm.gatling_angle * XM_2PI);
			g_world *= DirectX::XMMatrixTranslation(c.c_offset[0], c.c_offset[1], c.c_offset[2]);
			g_world *= DirectX::XMMatrixScaling(c.c_scale, c.c_scale, c.c_scale);
			g_world *= g_camera.GetWorldMatrix();

			V(g_gameEffect.worldEV->SetMatrix((float*)&g_world));

			worldViewProj = g_world * view * proj;
			V(g_gameEffect.worldViewProjectionEV->SetMatrix((float*)&worldViewProj));

			inverseTransposedWorld = DirectX::XMMatrixTranspose(DirectX::XMMatrixInverse(nullptr, g_world));
			V(g_gameEffect.worldNormalsEV->SetMatrix((float*)&inverseTransposedWorld));

			g_meshes.at(c.c_name)->render(pd3dImmediateContext, g_gameEffect.meshPass1,
				g_gameEffect.diffuseEV, g_gameEffect.specularEV, g_gameEffect.glowEV);
			break;

		case 2:
			if (!g_drawColorObjects)
				break;

			g_world *= DirectX::XMMatrixScaling(c.c_scale, c.c_scale, c.c_scale);
			g_world *= DirectX::XMMatrixRotationY(c.c_rotation[1]);
			g_world *= DirectX::XMMatrixRotationZ(c.c_rotation[2]);
			g_world *= DirectX::XMMatrixRotationX(c.c_rotation[0]);
			g_world *= DirectX::XMMatrixTranslation(c.c_offset[0], c.c_offset[1], c.c_offset[2]);

			V(g_gameEffect.worldEV->SetMatrix((float*)&g_world));

			worldViewProj = g_world * view * proj;
			V(g_gameEffect.worldViewProjectionEV->SetMatrix((float*)&worldViewProj));

			inverseTransposedWorld = DirectX::XMMatrixTranspose(DirectX::XMMatrixInverse(nullptr, g_world));
			V(g_gameEffect.worldNormalsEV->SetMatrix((float*)&inverseTransposedWorld));

			float col[4];
			for (int i = 0; i < 3; i++)
				col[i] = c.c_color[i];
			col[3] = 1;
			g_gameEffect.glowColor->SetFloatVector((float*)&col);

			g_meshes.at(c.c_name)->render(pd3dImmediateContext, g_gameEffect.meshPassGlow,
				g_gameEffect.diffuseEV, g_gameEffect.specularEV, g_gameEffect.glowEV);
			break;
		}
	}

	// Render Enemies
	for (Enemy e : gm_enemies) {
		XMMATRIX g_world = DirectX::XMMatrixIdentity();
		g_world *= DirectX::XMMatrixScaling(e.type.c_scale, e.type.c_scale, e.type.c_scale);
		g_world *= DirectX::XMMatrixRotationY(e.type.c_rotation[1]);
		g_world *= DirectX::XMMatrixRotationZ(e.type.c_rotation[2]);
		g_world *= DirectX::XMMatrixRotationX(e.type.c_rotation[0]);
		g_world *= DirectX::XMMatrixTranslation(e.type.c_offset[0], e.type.c_offset[1], e.type.c_offset[2]);

		g_world *= DirectX::XMMatrixRotationY(e.rotation[1]);
		g_world *= DirectX::XMMatrixRotationZ(e.rotation[2]);
		g_world *= DirectX::XMMatrixRotationX(e.rotation[0]);
		g_world *= DirectX::XMMatrixTranslation(e.position[0], e.position[1], e.position[2]);

		V(g_gameEffect.worldEV->SetMatrix((float*)&g_world));

		worldViewProj = g_world * view * proj;
		V(g_gameEffect.worldViewProjectionEV->SetMatrix((float*)&worldViewProj));

		inverseTransposedWorld = DirectX::XMMatrixTranspose(DirectX::XMMatrixInverse(nullptr, g_world));
		V(g_gameEffect.worldNormalsEV->SetMatrix((float*)&inverseTransposedWorld));

		g_meshes.at(e.type.c_meshName)->render(pd3dImmediateContext, g_gameEffect.meshPass1,
			g_gameEffect.diffuseEV, g_gameEffect.specularEV, g_gameEffect.glowEV);
	}


	if (!particles.empty())
	{
		XMFLOAT3 cam;
		XMStoreFloat3(&cam, g_camera.GetEyePt());
		XMFLOAT3 ahead;
		XMStoreFloat3(&ahead, g_camera.GetWorldAhead());

		particles.sort([cam, ahead](Particle& a, Particle& b)
		{
			XMFLOAT3 va = a.pos;
			XMFLOAT3 vb = b.pos;

			float da = va.z - 10; // Why the fuck would I have to substract 10? Idk, but it works...
			float db = vb.z - 10;

			if (a.parent == 0) {
				va.x -= cam.x;
				va.y -= cam.y;
				va.z -= cam.z;
				da = va.x * ahead.x + va.y * ahead.y + va.y * ahead.y;
			}
			if (b.parent == 0) {
				vb.x -= cam.x;
				vb.y -= cam.y;
				vb.z -= cam.z;
				db = vb.x * ahead.x + vb.y * ahead.y + vb.y * ahead.y;
			}

			return da > db;
		});

		std::vector<SpriteVertex> g_sprites;
		for (auto p : particles)
		{
			g_sprites.push_back({ p.pos, p.radius, p.age, p.alpha, p.animType, p.textureIndex, p.parent });
		}

		g_SpriteRenderer->renderSprites(pd3dImmediateContext, g_sprites, g_camera);
	}

	float bars[2] = { gm.gatling_heat, gm.plasma_power };
	bool colored[2] = { gm.gatling_overHeat, gm.plasma_power >= .99 };
	if (g_drawCockpit) g_HudRenderer->render(pd3dImmediateContext, bars, colored, g_camera, fElapsedTime);

	DXUT_BeginPerfEvent(DXUT_PERFEVENTCOLOR, L"HUD / Stats");
	V(g_hud.OnRender(fElapsedTime));
	V(g_sampleUI.OnRender(fElapsedTime));
	RenderText();
	DXUT_EndPerfEvent();

	static DWORD dwTimefirst = GetTickCount();
	if (GetTickCount() - dwTimefirst > 5000) {
		OutputDebugString(DXUTGetFrameStats(DXUTIsVsyncEnabled()));
		OutputDebugString(L"\n");
		dwTimefirst = GetTickCount();
	}
}

//--------------------------------------------------------------------------------------
// GAME MECHANICS
//--------------------------------------------------------------------------------------

void EnemyBehavior(float fElapsedTime)
{
	for (auto &enemy : gm_enemies) {
		enemy.pointForward();
		enemy.move(fElapsedTime);
	}

	// Handle enemy spawning and despawning
	DespawnEnemies();
	if (gm.spawnEnemies) {
		if (gm.enemySpawnCooldown < -500)
			gm.enemySpawnCooldown = cparser.getCSpawnParameters().c_downTime;
		gm.enemySpawnCooldown -= fElapsedTime;
		if (gm.enemySpawnCooldown <= 0) {
			gm.enemySpawnCooldown = cparser.getCSpawnParameters().c_timeInterval;
			SpawnEnemy();
		}
	}

	// Check for collisions
	{
		for (Particle &p : particles)
		{
			for (Enemy &e : gm_enemies)
			{
				float distanceSQ
					= pow(e.position[0] - p.pos.x, 2)
					+ pow(e.position[1] - p.pos.y, 2)
					+ pow(e.position[2] - p.pos.z, 2);

				if (distanceSQ < pow(e.type.c_size / 2 + p.radius.x, 2) && p.damage > 0.001)
				{
					e.hp -= p.damage;
					p.age = 10;
					Particle explosion = Particle(p.pos, { 5, 5 }, 4, 0, 2, 0, { e.velocity[0], e.velocity[1], e.velocity[2] });
					explosion.animType = 1;
					explosion.friction = 2;
					particles.push_back(explosion);
				}
			}

			if (abs(p.pos.x) > 1000 || abs(p.pos.y) > 1000 || abs(p.pos.z) > 1000)
				p.age = 10;
		}
	}
}

void Explode(XMFLOAT3 pos, float size)
{
	Particle mainParticle = Particle(pos, { size, size }, 4, 0, 0.5, 0);
	mainParticle.animType = 2;
	particles.push_back(mainParticle);

	int particleAmount = gm.explosion_particles * size;

	for (int i = 0; i < particleAmount; i++)
	{
		float phi = (float)rand() / RAND_MAX * XM_2PI;
		float theta = (float)rand() / RAND_MAX * XM_PI;

		float speed = size * gm.explosion_speed;
		XMFLOAT3 vel = { sinf(theta) * cosf(phi), sinf(theta) * sinf(phi), cosf(theta) };

		float size = ((float)rand() / RAND_MAX - 0.5f) * 0.5f + 1.f;

		Particle par = Particle(pos, { size, size }, 5, 0, 0.8, 0);

		par.vel = {
			vel.x * speed * (((float)rand() / RAND_MAX - 0.5f) * 0.5f + 1.f),
			vel.y * speed * (((float)rand() / RAND_MAX - 0.5f) * 0.5f + 1.f),
			vel.z * speed * (((float)rand() / RAND_MAX - 0.5f) * 0.5f + 1.f) };

		par.acc = { 0, -20, 0 };
		par.animType = 3;
		par.friction = 0.1;

		particles.push_back(par);
	}
}

void SpawnEnemy()
{
	Enemy newEnemy;
	int typeAmount = cparser.getCEnemies().size();
	std::string name = cparser.getCEnemyTypes().at(rand() % typeAmount);
	c_EnemyType etype = cparser.getCEnemies().at(name);
	newEnemy.type = etype;
	newEnemy.hp = etype.c_hp;

	float approachingfromAngle = (float)rand() / RAND_MAX * XM_2PI;
	float approachingtoAngle = (float)rand() / RAND_MAX * XM_2PI;

	newEnemy.position[0] = cos(approachingfromAngle) * gm_spawnConstants.gm_spawnRadius;
	newEnemy.position[1] = ((float)rand() / RAND_MAX *
		(cparser.getCSpawnParameters().c_maxHeight - cparser.getCSpawnParameters().c_minHeight)
		+ cparser.getCSpawnParameters().c_minHeight) * cparser.getTerrainHeigth();
	newEnemy.position[2] = sin(approachingfromAngle) * gm_spawnConstants.gm_spawnRadius;

	float approachingtoPoint[3];
	approachingtoPoint[0] = cos(approachingtoAngle) * gm_spawnConstants.gm_targetRadius;
	approachingtoPoint[1] = newEnemy.position[1];
	approachingtoPoint[2] = sin(approachingtoAngle) * gm_spawnConstants.gm_targetRadius;

	float dirLen = 0.0f;
	for (int i = 0; i < 3; i++) dirLen += pow((newEnemy.velocity[i] = approachingtoPoint[i] - newEnemy.position[i]), 2);
	dirLen = sqrtf(dirLen);
	for (int i = 0; i < 3; i++) newEnemy.velocity[i] *= newEnemy.type.c_speed / dirLen;

	newEnemy.smoothFac = 1.f;

	for (int i = 0; i < 3; i++) newEnemy.rotation[i] = 0;
	newEnemy.pointForward();

	gm_enemies.push_back(newEnemy);
}

void DespawnEnemies()
{
	for (auto &enemy : gm_enemies)
	{
		if (pow(enemy.position[0], 2) + pow(enemy.position[2], 2) > pow(gm_spawnConstants.gm_despawnRadius, 2))
			enemy.hp = 0;

		if (enemy.hp <= 0)
		{
			float size = gm.explosion_size * enemy.type.c_size;
			Explode({ enemy.position[0], enemy.position[1], enemy.position[2] }, size);
		}
	}
	gm_enemies.remove_if([](Enemy e) { return e.hp <= 0; });
}

void Shoot(int gun)
{
	float acc;

	Particle par;

	switch (gun)
	{
	case 0b00: // Gatling Shoot
		XMFLOAT3 forward;
		XMStoreFloat3(&forward, g_camera.GetWorldAhead());
		XMFLOAT3 right;
		XMStoreFloat3(&right, g_camera.GetWorldRight());
		XMFLOAT3 up;
		XMStoreFloat3(&up, g_camera.GetWorldUp());
		XMFLOAT3 pos;
		XMStoreFloat3(&pos, g_camera.GetEyePt());

		XMFLOAT3 start;
		start.x = pos.x - 2.7 * right.x + .3 * up.x + 8.8 * forward.x;
		start.y = pos.y - 2.7 * right.y + .3 * up.y + 8.8 * forward.y;
		start.z = pos.z - 2.7 * right.z + .3 * up.z + 8.8 * forward.z;

		forward.x *= cparser.getCGun("Gatling").c_projectileSpeed;
		forward.y *= cparser.getCGun("Gatling").c_projectileSpeed;
		forward.z *= cparser.getCGun("Gatling").c_projectileSpeed;

		forward.x += (float)rand() / RAND_MAX - 0.5;
		forward.y += (float)rand() / RAND_MAX - 0.5;
		forward.z += (float)rand() / RAND_MAX - 0.5;

		acc = -20 * cparser.getCGun("Gatling").c_gravityFac;

		par = Particle(start, { 1, 1 }, 0, 0, .00001,
			cparser.getCGun("Gatling").c_damage, forward, { 0, acc , 0 });
		particles.push_back(par);

		par = Particle({ -2.6, .3, 8.5 }, { .5, .6 }, 1, 1, 10, 0);
		par.animType = 1;
		particles.push_back(par);
		break;

	case 0b01: // Gatling Smoke
		break;

	case 0b10: // Plasma
		XMStoreFloat3(&forward, g_camera.GetWorldAhead());
		XMStoreFloat3(&right, g_camera.GetWorldRight());
		XMStoreFloat3(&up, g_camera.GetWorldUp());
		XMStoreFloat3(&pos, g_camera.GetEyePt());

		start.x = pos.x + 2.7 * right.x + .3 * up.x + 8.6 * forward.x;
		start.y = pos.y + 2.7 * right.y + .3 * up.y + 8.6 * forward.y;
		start.z = pos.z + 2.7 * right.z + .3 * up.z + 8.6 * forward.z;

		forward.x *= cparser.getCGun("Plasma").c_projectileSpeed;
		forward.y *= cparser.getCGun("Plasma").c_projectileSpeed;
		forward.z *= cparser.getCGun("Plasma").c_projectileSpeed;

		forward.x += (float)rand() / RAND_MAX - 0.5;
		forward.y += (float)rand() / RAND_MAX - 0.5;
		forward.z += (float)rand() / RAND_MAX - 0.5;

		acc = -10 * cparser.getCGun("Plasma").c_gravityFac;

		par = Particle(start, { 3, 3 }, 2, 0, .00001,
			cparser.getCGun("Plasma").c_damage, forward, { 0, acc, 0 });

		particles.push_back(par);
		break;
	}
}

void GunBehavior(float fElapsedTime)
{
	bool input = !DXUTIsKeyDown(VK_LCONTROL) && !g_settingsDlg.IsActive();
	bool lB = DXUTIsMouseButtonDown(VK_LBUTTON) && input;
	bool rB = DXUTIsMouseButtonDown(VK_RBUTTON) && input;
	if (lB && (gm.gatling_speed_mode == 1 || !gm.gatling_overHeat)) gm.gatling_speed_mode = 1;
	else    gm.gatling_speed_mode = 2;

	bool inOverHeat = gm.gatling_heat > .8;

	if (inOverHeat)
		gm.gatling_overHeatPercent += fElapsedTime / 2;
	else gm.gatling_overHeatPercent -= fElapsedTime;

	gm.gatling_overHeatPercent = CLAMP(gm.gatling_overHeatPercent);

	if (gm.gatling_overHeatPercent >= .99)
		gm.gatling_overHeat = true;

	if (gm.gatling_heat <= .01)
		gm.gatling_overHeat = false;

	switch (gm.gatling_speed_mode) {
	case 1:
		gm.gatling_speed += fElapsedTime * 10;
		gm.gatling_speed = min(gm.gatling_speed, gm.gatling_speed_max);
		break;

	case 2:
		gm.gatling_speed -= fElapsedTime * 10;

		if (gm.gatling_speed < 0.001) {
			gm.gatling_speed = 0;
			gm.gatling_speed_mode = 0;
		}
		break;
	}

	gm.gatling_angle += gm.gatling_speed * fElapsedTime;
	gm.gatling_heat += gm.gatling_speed * fElapsedTime / 5;
	gm.gatling_heat -= (gm.gatling_speed_max - gm.gatling_speed) * fElapsedTime / 2;
	gm.gatling_heat = CLAMP(gm.gatling_heat); // Clamp


	if (gm.gatling_angle >= 1.0f)
	{
		gm.gatling_angle = std::fmod(gm.gatling_angle, 1.0f);
		gm.gatling_nextShot %= 6;
	}

	if (gm.gatling_speed >= 0.99 * gm.gatling_speed_max) // is shooting
	{
		if (6 * gm.gatling_angle > gm.gatling_nextShot)
		{
			gm.gatling_nextShot++;

			if (!gm.gatling_overHeat)
				Shoot(0b00);
			else
				Shoot(0b01);
		}
	}

	if (rB && gm.plasma_power >= .99)
	{
		Shoot(0b10);
		gm.plasma_power = 0;
	}

	gm.plasma_power += fElapsedTime * 1 / cparser.getCGun("Plasma").c_cooldown;
	gm.plasma_power = CLAMP(gm.plasma_power);

}

void ParticleBehavior(float fElapsedTime)
{
	for (auto &p : particles)
	{
		p.age += fElapsedTime * p.ageSpeed;

		p.pos.x += fElapsedTime * p.vel.x;
		p.vel.x += fElapsedTime * p.acc.x;
		p.pos.y += fElapsedTime * p.vel.y;
		p.vel.y += fElapsedTime * p.acc.y;
		p.pos.z += fElapsedTime * p.vel.z;
		p.vel.z += fElapsedTime * p.acc.z;

		float fricFac = p.friction * fElapsedTime;
		p.vel.x = fricFac * gm.windDir.x + (1 - fricFac) * p.vel.x;
		p.vel.y = fricFac * gm.windDir.y + (1 - fricFac) * p.vel.y;
		p.vel.z = fricFac * gm.windDir.z + (1 - fricFac) * p.vel.z;
	}

	particles.remove_if([](Particle p) {return p.age >= 1; });
}