#include "ConfigParser.h"

int ConfigParser::load(std::string filename)
{
	std::ifstream ifs;
	ifs.open(filename);
	if (!ifs.is_open()) return -1;

	while (ifs.good()) {

		std::string keyword;
		ifs >> keyword;

		if (keyword == "spinning")				ifs >> spinning;
		else if (keyword == "spinSpeed")		ifs >> spinSpeed;
		else if (keyword == "backgroundColor")	ifs >> backgroundColor.r >> backgroundColor.g >> backgroundColor.b;
		else if (keyword == "TerrainWidth")		ifs >> terrainWidth;
		else if (keyword == "TerrainDepth")		ifs >> terrainDepth;
		else if (keyword == "TerrainHeight")	ifs >> terrainHeight;
		else if (keyword == "TerrainPath")      ifs >> terrainPaths[0] >> terrainPaths[1] >> terrainPaths[2];
		else if (keyword == "Spawn")
			ifs >> c_spawnParameters.c_downTime >> c_spawnParameters.c_timeInterval
			>> c_spawnParameters.c_minHeight >> c_spawnParameters.c_maxHeight;
		else if (keyword == "Mesh") {
			std::string name;
			ifs >> name;
			std::vector<std::string> paths{ "", "", "", "" };
			ifs >> paths[0] >> paths[1] >> paths[2] >> paths[3];
			meshPaths.insert(std::make_pair(name, paths));
		}
		else if (keyword == "SpriteTexture") {
			std::string textureName;
			ifs >> textureName;
			c_spriteTextures.push_back(std::wstring(textureName.begin(), textureName.end()));
		}
		else if (keyword == "CockpitObject" || keyword == "GroundObject") {
			c_Object co(keyword);
			ifs >> co.c_name >> co.c_scale >> co.c_rotation[0] >> co.c_rotation[1] >> co.c_rotation[2]
				>> co.c_offset[0] >> co.c_offset[1] >> co.c_offset[2];
			for (int i = 0; i < 3; i++)
				co.c_rotation[i] *= 0.01745329251f;
			c_Objects.push_back(co);
		}
		else if (keyword == "Gun")
		{
			std::string name;
			ifs >> name;
			c_Gun gun;
			ifs >> gun.c_projectileSpeed >> gun.c_cooldown >> gun.c_damage >> gun.c_gravityFac;
			c_guns.insert(std::make_pair(name, gun));
		}
		else if (keyword == "ColorObject") {
			c_Object co(keyword);
			ifs >> co.c_name
				>> co.c_color[0] >> co.c_color[1] >> co.c_color[2]
				>> co.c_scale >> co.c_rotation[0] >> co.c_rotation[1] >> co.c_rotation[2]
				>> co.c_offset[0] >> co.c_offset[1] >> co.c_offset[2];
			for (int i = 0; i < 3; i++)
				co.c_rotation[i] *= 0.01745329251f;
			c_Objects.push_back(co);
		}
		else if (keyword == "EnemyType") {
			c_EnemyType ce;
			std::string c_type;
			ifs >> c_type >> ce.c_hp >> ce.c_size >> ce.c_speed >> ce.c_meshName >> ce.c_scale
				>> ce.c_rotation[0] >> ce.c_rotation[1] >> ce.c_rotation[2]
				>> ce.c_offset[0] >> ce.c_offset[1] >> ce.c_offset[2];
			for (int i = 0; i < 3; i++)
				ce.c_rotation[i] *= 0.01745329251f;
			c_EnemyTypes.push_back(c_type);
			c_Enemies.insert(std::make_pair(c_type, ce));
		}
		else if (keyword == "");
		else if (keyword[0] == '/');
		else std::cout << "Unknown keyword: " << keyword << std::endl;
	}
	ifs.close();
	return 0;
}

int ConfigParser::print(std::string filename)
{
	std::ifstream ifs(filename, std::ifstream::in);
	if (!ifs.is_open()) return -1;
	char c = ifs.get();
	while (ifs.good()) {
		std::cout << c;
		c = ifs.get();
	}
	ifs.close();
	return 0;
}

float ConfigParser::getSpinning() { return spinning; }
float ConfigParser::getSpinSpeed() { return spinSpeed; }
ConfigParser::Color ConfigParser::getBackgroundColor() { return backgroundColor; }
std::string ConfigParser::getTerrainPath(int i) { return terrainPaths[i]; }
float ConfigParser::getTerrainWidth() { return terrainWidth; }
float ConfigParser::getTerrainDepth() { return terrainDepth; }
float ConfigParser::getTerrainHeigth() { return terrainHeight; }
std::string ConfigParser::getMeshPath(std::string name, int i) { return "resources\\" + meshPaths.at(name)[i]; }
std::vector<std::string> ConfigParser::getMeshNames()
{
	std::vector<std::string> v;
	v.reserve(meshPaths.size());
	for (auto p : meshPaths)
		v.push_back(p.first);
	return v;
}
std::vector<c_Object> ConfigParser::getCObjects() { return c_Objects; }
std::map<std::string, c_EnemyType> ConfigParser::getCEnemies() { return c_Enemies; }
std::vector<std::string> ConfigParser::getCEnemyTypes() { return c_EnemyTypes; }
c_EnemyType ConfigParser::getCEnemyType(std::string type) { return c_Enemies.at(type); }
c_SpawnParameters ConfigParser::getCSpawnParameters() { return c_spawnParameters; }
std::vector<std::wstring> ConfigParser::getCSpriteTextures() { return c_spriteTextures; }
c_Gun ConfigParser::getCGun(std::string name) { return c_guns.at(name); }