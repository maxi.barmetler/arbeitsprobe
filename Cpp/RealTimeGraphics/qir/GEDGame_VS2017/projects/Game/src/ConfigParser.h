#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include <list>

#ifndef CONFIGPARSER_H
#define CONFIGPARSER_H
struct c_Object
{
	c_Object(std::string type) : c_type(-1)
	{
		int i = 0;
		for (std::string t : {"GroundObject", "CockpitObject", "ColorObject"})
		{
			if (type == t) c_type = i;
			i++;
		}
	}

	int c_type;
	std::string c_name;
	float c_scale;
	float c_rotation[3];
	float c_offset[3];
	float c_color[3];
};

struct c_EnemyType
{
	int c_hp;
	int c_size;
	int c_speed;
	std::string c_meshName;
	float c_scale;
	float c_rotation[3];
	float c_offset[3];
};

struct c_SpawnParameters
{
	float c_downTime;
	float c_timeInterval;
	float c_minHeight;
	float c_maxHeight;
};

struct c_Gun
{
	float c_projectileSpeed;
	float c_cooldown;
	float c_damage;
	float c_gravityFac;
};
#endif

class ConfigParser
{
public:
	struct Color
	{
		float r, g, b;
		std::string ToString() { return std::to_string(r) + " " + std::to_string(g) + " " + std::to_string(b); }
	};

private:
	float spinning;
	float spinSpeed;
	Color backgroundColor;
	std::vector<std::string> terrainPaths{ "", "", "" };
	float terrainWidth;
	float terrainDepth;
	float terrainHeight;
	std::map<std::string, std::vector<std::string>> meshPaths;
	std::vector<c_Object> c_Objects;
	std::vector<std::string> c_EnemyTypes;
	std::map<std::string, c_EnemyType> c_Enemies;
	c_SpawnParameters c_spawnParameters;
	std::vector<std::wstring> c_spriteTextures;
	std::map<std::string, c_Gun> c_guns;

public:
	int load(std::string filename);
	int print(std::string filename);

	float getSpinning(), getSpinSpeed();
	Color getBackgroundColor();
	std::string getTerrainPath(int i);
	float getTerrainWidth(), getTerrainDepth(), getTerrainHeigth();
	std::string getMeshPath(std::string name, int i);
	std::vector<std::string> getMeshNames();
	std::vector<c_Object> getCObjects();
	std::map<std::string, c_EnemyType> getCEnemies();
	std::vector<std::string> getCEnemyTypes();
	c_EnemyType getCEnemyType(std::string type);
	c_SpawnParameters getCSpawnParameters();
	std::vector<std::wstring> getCSpriteTextures();
	c_Gun getCGun(std::string name);
};

extern ConfigParser cparser;