#pragma once
#include "DXUT.h"
#include "d3dx11effect.h"
#include "ConfigParser.h"

class Terrain
{
public:
	Terrain(void);
	~Terrain(void);

	HRESULT create(ID3D11Device* device, ConfigParser cparser);
	void destroy();

	void render(ID3D11DeviceContext* context, ID3DX11EffectPass* pass);

	float center;


private:
	Terrain(const Terrain&);
	Terrain(const Terrain&&);
	void operator=(const Terrain&);

	// Terrain rendering resources
	// ID3D11Buffer*                        vertexBuffer;      // The terrain's vertices
	ID3D11Buffer*                           indexBuffer;	   // The terrain's triangulation
	ID3D11Texture2D*                        diffuseTexture;    // The terrain's material color for diffuse lighting
	ID3D11ShaderResourceView*               diffuseTextureSRV; // Describes the structure of the diffuse texture to the shader stages
	ID3D11Texture2D*                        normalMap;         // The terrain's normal map
	ID3D11ShaderResourceView*               normalMapSRV;      // Describes the terrain's normal map to the shader stages
	ID3D11Buffer*                           heightBuffer;      // The terrain's height map
	ID3D11ShaderResourceView*               heightBufferSRV;   // Describes the terrain's height map to the shader stages

	// General resources
	ID3D11ShaderResourceView*               debugSRV;
};

